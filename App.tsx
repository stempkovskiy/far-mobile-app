import React, { useState} from 'react';
import * as Font from 'expo-font'
import { AppLoading } from 'expo'
import { AppNavigation } from './src/navigation/AppNavigation'
import { AuthState } from './src/context/auth/AuthState'
import { AppState } from './src/context/app/AppState'
import { FilterState } from './src/context/filter/FilterState'
import { THEME } from './src/theme';




async function loadAplication() {
  await Font.loadAsync({
    [THEME.JOSEFINSANS_BOILD]: require('./assets/fonts/JosefinSans-Bold.ttf'),
    [THEME.JOSEFINSANS_REGULAR]: require('./assets/fonts/JosefinSans-Regular.ttf'),
  })
}

export default function App() {

  const [isReady, setIsReady] = useState<boolean>(false)


  if (!isReady) {
    return <AppLoading
      startAsync={loadAplication}
      onError={error => console.log(error)}
      onFinish={() => setIsReady(true)} />
  }

  return (
    <AppState>
      <AuthState>
        <FilterState>
          <AppNavigation />
        </FilterState>
      </AuthState>
    </AppState>
  )

}

