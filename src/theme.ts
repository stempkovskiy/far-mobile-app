import { Favourite } from "./screens/favourite/Favourite";

export const THEME = {
    COLOR_ERROR: '#fc0313',
    COLOR_SUCCESS: '#42f545',
    PADING_LEFT_RIGHT_INPUT: 30,
    BTN_BORDER_RADIUS: 10,
    BORDER_BOTTOM_COLOR: '#707070',
    BTN_BACKGROUND_COLOR: '#5663FF',
    BTN_BACKGROUND_DISABLED: '#696D9F',
    JOSEFINSANS_BOILD: 'josefinSans-bold',
    JOSEFINSANS_REGULAR: 'josefinSans-regular',
    WHITE_COLOR: '#FFFFFF',
    GRADIENTS: {
        KITCHEN_INFO: ['#FF5673', '#FF8C48'],
        DISTANCE_INFO: '#848DFF',
    },
    TEXT_COLOR: '#8A98BA',
    TEXT_STATE_OPEN: '#4CD864',
    TEXT_STATE_CLOSE: '#FF4A40',
    TEXT_STATE_UNKNOWN: '#FAA32E',
    BORDER_INPUT_ERROR: {
        borderColor: 'red',
        borderStyle: 'solid',
        borderWidth: 1,
        padding: 9
    },
    BORDER_INPUT_SUCCSES: {
        borderColor: '#59FA2E',
        borderStyle: 'solid',
        borderWidth: 1,
        padding: 9
    },
    HOME_IMG: require('../assets/img/tabs/home.png'),
    HOME_ACTIVE_IMG: require('../assets/img/tabs/home_active.png'),
    FAVOURITE_IMG: require('../assets/img/tabs/favorite.png'),
    FAVOURITE_IMG_WHITE: require('../assets/img/tabs/favorite_white_stroke.png'),
    FAVOURITE_ACTIVE_IMG: require('../assets/img/tabs/favorite_active.png'),
    NOTIFICATION_IMG: require('../assets/img/tabs/notification.png'),
    NOTIFICATION_ACTIVE_IMG: require('../assets/img/tabs/notification_active.png'),
    PROFILE_IMG: require('../assets/img/tabs/profile.png'),
    PROFILE_ACTIVE_IMG: require('../assets/img/tabs/profile_active.png'),
}