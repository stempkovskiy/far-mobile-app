import React, { useContext, useState } from 'react'
import { StyleSheet, View } from 'react-native'
import { Search } from '../../components/UI/Search'
import { THEME } from '../../theme'
import { FlatList } from 'react-native-gesture-handler'
import { CardRestaurans } from '../../components/CardRestaurans'
import { AppContext } from '../../context/app/appContenxt'
import { TextRegular } from '../../components/UI/TextRegular'
import { FilterContext } from '../../context/filter/filterContext'
import { useRoute } from '@react-navigation/native'
import { Restauran } from '../../api/restauran'

export type FilterBody = {
    path: {
        distance: number
        lastPosX: number
        transitionX: number
        width: number
    }
    rating: number
    categories: string[]
}

export interface ITrendingRestauransRouteData {
    restaurants: Restauran[]
}


export const TrendingRestaurants: React.FC = () => {
    const { restaurants } = useRoute().params as ITrendingRestauransRouteData
    const [data, setData] = useState(restaurants)
    const onChange = (text: string) => {
        if (text.trim()) {
            setData(data.filter(r => r.name.includes(text)))
        } else {
            setData(restaurants)
        }
    }


    return (
        <View style={styles.container}>
            <Search
                onChange={onChange}
                placeholder='Search'
                icon={true} />
            {
                data.length !== 0
                    ? <FlatList
                        style={{ width: '100%' }}
                        data={data}
                        showsVerticalScrollIndicator={false}
                        keyExtractor={item => item.id!.toString()}
                        renderItem={({ item }) =>
                            <CardRestaurans
                                style={{ marginLeft: 0, marginBottom: 20, width: '100%' }}
                                info={{ ...item }} />
                        }
                    />
                    : <View style={styles.notFound}>
                        <TextRegular text='Nothing found by filter' fontSize={20} color={THEME.TEXT_COLOR} />
                    </View>
            }
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        paddingHorizontal: 10,
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'center',
        backgroundColor: THEME.WHITE_COLOR,
    },
    notFound: {
        flex: 1,
        alignSelf: 'center',
        justifyContent: 'center',
        alignItems: 'center'
    }
})