import { AntDesign } from '@expo/vector-icons'
import React from 'react'
import { View, Image, StyleSheet } from 'react-native'
import { TextBold } from '../../../components/UI/TextBold'
import { TextRegular } from '../../../components/UI/TextRegular'
import { THEME } from '../../../theme'

type UserReviewType = {
    avatar: string,
    name: string,
    rating: number,
    review: string
}

export const ReviewUser: React.FC<UserReviewType> = ({avatar, name, rating, review}) => {
    return <View style={styles.container}>
        <Image
            source={{ uri: avatar }}
            style={styles.avatar} />
        <View style={{ flex: 1, justifyContent: 'space-evenly', paddingHorizontal: 3 }}>
            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                <TextBold text={name} fontSize={17} />
                <View style={styles.rating}>
                    <AntDesign name='star' color='#f0c21d' size={15} />
                    <TextBold text={`${rating}`} fontSize={15} />
                </View>
            </View>
            <TextRegular text={review} color={THEME.TEXT_COLOR} />
        </View>
    </View>
}


const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        flexDirection: 'row'
    },
    avatar: {
        width: 50,
        height: 50,
        borderRadius: 30,
        margin: 10
    },
    rating: { 
        flex: 0.25, 
        borderRadius: 5, 
        marginRight: 20, 
        backgroundColor: '#F6F7FF', 
        flexDirection: 'row', 
        justifyContent: 'space-evenly', 
        alignItems: 'center' 
    }
})