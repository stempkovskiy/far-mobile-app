import { FontAwesome } from '@expo/vector-icons'
import { LinearGradient } from 'expo-linear-gradient'
import React from 'react'
import { StyleSheet, View } from 'react-native'
import { TimeWork } from '../../../api/restauran'
import { TextBold } from '../../../components/UI/TextBold'
import { TextRegular } from '../../../components/UI/TextRegular'
import { THEME } from '../../../theme'
import { dailyTime, daliTimeEnum } from '../../../utils/restauransWork'

type InfoRestaurant = {
    name: string
    street: string
    kitchen: string
    distance: number
    rating: number,
    timeWorks: TimeWork[]
}

export const InfoRestaurant: React.FC<InfoRestaurant> =
    ({ name, street, kitchen, distance, rating, timeWorks }) => {

        const getDailyTimeText = () => {
            const response = dailyTime(timeWorks)
            if (response.state === daliTimeEnum.wokr) {
                timeWork('Open Now', response.currentTime!, THEME.TEXT_STATE_OPEN)
            } else if (response.state === daliTimeEnum.notWork) {
                return timeWork('Close Now', response.currentTime!, THEME.TEXT_STATE_CLOSE)
            } else {
                return <TextRegular text='Restaurant opening hours are not specified' />
            }
        }

        const timeWork = (text: string, time: TimeWork, color: string) => {
            return <View style={styles.timeWork}>
                <TextRegular text={text} color={color} />
                <TextRegular
                    style={{ marginHorizontal: 3 }}
                    text='daily time'
                    color={THEME.TEXT_COLOR} />
                <TextRegular
                    text={`${time.beginWork} am to ${time.endWork}pm`}
                    color={color} />
            </View>
        }



        return (
            <View style={{ flex: 0.4, paddingHorizontal: 15 }}>
                <View style={{ flex: 0.6, flexDirection: 'row' }} >
                    <View style={{ flexDirection: 'row', alignItems: 'center', flex: 1 }}>
                        <TextBold text={name} fontSize={18} />
                        <LinearGradient colors={THEME.GRADIENTS.KITCHEN_INFO} style={styles.kitchen}>
                            <TextRegular text={kitchen} color={THEME.WHITE_COLOR} fontSize={12} />
                        </LinearGradient>
                        <View style={styles.distance}>
                            <TextRegular text={`${distance} km`} color={THEME.WHITE_COLOR} fontSize={12} />
                        </View>
                    </View>
                    <View style={styles.rating}>
                        <FontAwesome name='star' color='#FFCC00' size={20} />
                        <TextBold text={`${rating}`} fontSize={17} color='#4E4F76' />
                    </View>
                </View>
                <View style={{ flex: 0.3 }} >
                    <TextRegular text={street} color={THEME.TEXT_COLOR} />
                </View>
                <View style={{ flex: 0.5 }} >
                    {
                        getDailyTimeText()
                    }
                </View>
            </View>
        )
    }

const styles = StyleSheet.create({
    kitchen: {
        paddingHorizontal: 4,
        paddingVertical: 1,
        borderRadius: 15,
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: 5
    },
    distance: {
        paddingHorizontal: 4,
        paddingVertical: 1,
        borderRadius: 15,
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: 5,
        backgroundColor: THEME.GRADIENTS.DISTANCE_INFO
    },
    rating: {
        flex: 0.2,
        borderRadius: 5,
        marginTop: 10,
        backgroundColor: '#F6F7FF',
        flexDirection: 'row',
        justifyContent: 'space-evenly',
        alignItems: 'center'
    },
    timeWork: {
        flexDirection: 'row'
    }
})