import { useRoute } from '@react-navigation/native'
import React from 'react'
import { StyleSheet, View, Image, Dimensions } from 'react-native'
import { Photo } from '../../../api/restauran'
import { TextRegular } from '../../../components/UI/TextRegular'
import { THEME } from '../../../theme'
import { getRandomInt, getRoundedDivNumber } from '../../../utils/restauransWork'

export interface IMenuPhotos {
    photos: Photo[]
}
export const MenuPhotos: React.FC = () => {
    const { photos } = useRoute().params as IMenuPhotos

    const line = (photos: Photo[], key: number) => {
        return <View
            key={key}
            style={styles.line}>
            {
                photos.map(photo =>
                    <Image
                        key={photo.id}
                        style={styles.imageLine}
                        source={{ uri: photo.path }}
                    />)
            }
        </View>
    }

    const block = (photos: Photo[], key: number) => {
        const randomSizeImg = 1
        //getRandomInt(0, photos.length)
        return <View
            key={key}
            style={styles.block}>
            {
                randomSizeImg !== 1
                    ? photos.map((photo, index) => {
                        return <Image
                            style={{
                                borderRadius: 15,
                                height: randomSizeImg === index ? '99%' : '48%',
                                width: randomSizeImg === index && randomSizeImg !== 1 ? '99%' : '48%'
                            }}
                            source={{ uri: photo.path }}
                        />
                    })
                    : <>
                        <View style={{ flex: 1, height: '100%', padding: 3, justifyContent: 'space-evenly' }} >
                            <Image
                                style={{
                                    borderRadius: 15,
                                    height: '48%',
                                    width: '100%'
                                }}
                                source={{ uri: photos[0].path }}
                            />
                            <Image
                                style={{
                                    borderRadius: 15,
                                    height: '48%',
                                    width: '100%'
                                }}
                                source={{ uri: photos[1].path }}
                            />
                        </View>
                        <View style={{ flex: 1, height: '100%', padding: 3 }} >
                            <Image
                                style={{
                                    borderRadius: 15,
                                    width: '100%',
                                    flex: 1
                                }}
                                source={{ uri: photos[2].path }}
                            />
                        </View>
                    </>
            }
        </View>
    }

    const photosMenu = () => {
        const countRow = getRoundedDivNumber(photos.length, 3)
        let start = 0
        let end = 0
        return new Array(countRow)
            .fill(1)
            .map((_, index) => {
                start = end
                end += 3
                const array = photos.slice(start, end)
                if (index % 2 && array.length > 2) {
                    return block(array, index)
                } else {
                    return line(array, index)
                }
            })
    }

    return <View style={styles.container}>
        {
            photosMenu()
        }
    </View>
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: THEME.WHITE_COLOR
    },
    line: {
        width: '100%',
        flexDirection: 'row',
        height: 120,
    },
    block: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        height: 250,
        justifyContent: 'space-around'
    },
    imageLine: {
        margin: 3,
        flex: 1,
        borderRadius: 15,
    }
})