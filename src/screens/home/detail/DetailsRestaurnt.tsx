import React, { useState, useRef, useContext, useEffect } from 'react'
import {
    StyleSheet,
    View,
    ImageBackground,
    Dimensions,
    FlatList,
    TouchableOpacity,
    Animated,
    PanResponder,
    Image,
    Easing
} from 'react-native'
import { useRoute, useNavigation } from '@react-navigation/native'
import { Ionicons, Feather } from '@expo/vector-icons'
import { THEME } from '../../../theme'
import { LinearGradient } from 'expo-linear-gradient'
import { InfoLine } from '../../../components/InfoLine'
import { TextBold } from '../../../components/UI/TextBold'
import { categoryMokData } from '../../../mock/category'
import { AppContext } from '../../../context/app/appContenxt'
import { InfoRestaurant } from './infoRestaurant'
import { Restauran } from '../../../api/restauran'
import { ReviewUser } from './ReviewUser'
import { IMenuPhotos } from './MenuPhotos'
import { ListPageInfoType } from '../../../components/ListPageInfo'


export const DetalisRestauran: React.FC = () => {
    const restaurant = useRoute().params as Restauran
    const { restaurants } = useContext(AppContext)
    const [isFavorite, setFavorite] = useState(false)
    const pan = useRef(new Animated.ValueXY()).current
    const isUp = useRef(false)
    const panResponder = useState(
        PanResponder.create({
            onMoveShouldSetPanResponderCapture: () => true,
            onPanResponderGrant: (e, g) => {
                if (isUp.current) {
                    pan.setOffset({
                        x: 0,
                        y: -150
                    })
                } else {
                    pan.setOffset({
                        x: 0,
                        y: 0
                    })
                }
            },
            onPanResponderMove: (e, g) => {
                if (isUp.current) {
                    if (g.dy >= 0 && g.dy <= 150) {
                        pan.y.setValue(g.dy)
                    }
                } else {
                    if (g.dy >= -150 && g.dy <= 0) {
                        pan.y.setValue(g.dy)
                    }
                }
            },
            onPanResponderRelease: (e, g) => {
                if (isUp.current) {
                    if (g.dy >= 0) {
                        Animated.timing(pan, {
                            toValue: 150,
                            duration: 100,
                            useNativeDriver: false
                        }).start(() => isUp.current = false)
                    }
                } else {
                    if (g.dy >= -150 && g.dy <= 0) {
                        Animated.timing(pan, {
                            toValue: -150,
                            easing: Easing.linear,
                            duration: 100,
                            useNativeDriver: false
                        }).start(() => isUp.current = true)
                    } else if (g.dy <= -150) {
                        Animated.timing(pan, {
                            toValue: -150,
                            easing: Easing.linear,
                            duration: 100,
                            useNativeDriver: false
                        }).start(() => isUp.current = true)
                    }
                }
            }
        })
    )[0]

    const { navigate, goBack } = useNavigation()
    const { addFavourite, removeFavourite } = useContext(AppContext)

    useEffect(() => {
        setFavorite(restaurants.find(r => r.id === restaurant.id)?.isFavouvarit!)
    }, [])

    const changeFavoriteState = () => {
        if (isFavorite) {
            removeFavourite(restaurant)
            setFavorite(false)
        } else {
            addFavourite(restaurant)
            setFavorite(true)
        }
    }


    return (
        <View style={styles.content}>
            <ImageBackground style={styles.header} source={{ uri: restaurant.photos[0].path }} >
                <LinearGradient
                    colors={['rgba(0, 0, 0, 0.58)', 'transparent']}
                    style={styles.headerWrapGradient}>
                    <View style={styles.headerWrapGradientTitle}>
                        <TouchableOpacity onPress={goBack} style={styles.headerBtnBack}>
                            <Ionicons name='ios-arrow-back' color={THEME.WHITE_COLOR} size={30} />
                        </TouchableOpacity>
                        <View style={styles.headerWrapGradientTitleShared}>
                            <TouchableOpacity onPress={changeFavoriteState}>
                                <Image source={
                                    isFavorite
                                        ? THEME.FAVOURITE_ACTIVE_IMG
                                        : THEME.FAVOURITE_IMG_WHITE
                                }
                                    resizeMode='center'
                                    style={{ width: 30, height: 30 }} />
                            </TouchableOpacity>
                        </View>
                    </View>
                </LinearGradient>
            </ImageBackground>
            <Animated.View
                style={{ ...styles.footer, transform: [{ translateY: pan.y }] }}
                {...panResponder.panHandlers}>
                <View style={styles.footerTitle}>

                    <InfoRestaurant
                        distance={restaurant.km}
                        kitchen={restaurant.kitchen}
                        name={restaurant.name}
                        rating={restaurant.rating}
                        street={restaurant.street}
                        timeWorks={restaurant.timeWorks}
                    />

                    <View style={{ flex: 0.6 }} >
                        <View style={{ flex: 0.3 }} >
                            <InfoLine
                                onPress={() => navigate('Menu&Photos', {
                                    photos: restaurant.menuPhoto
                                } as IMenuPhotos)}
                                title='Menu & Photos'
                                info={`See all (${restaurant.menuPhoto.length})`}
                                style={{ marginHorizontal: 20 }}
                            />
                        </View>
                        <FlatList
                            style={{ flex: 0.4 }}
                            data={restaurant.menuPhoto}
                            horizontal={true}
                            showsHorizontalScrollIndicator={false}
                            keyExtractor={item => item.id}
                            renderItem={({ item, index }) => {
                                if (index !== categoryMokData.length - 1) {
                                    return <Image
                                        source={{ uri: item.path }}
                                        style={styles.menuPhoto} />
                                } else {
                                    return <Image
                                        source={{ uri: item.path }}
                                        style={{ ...styles.menuPhoto, marginRight: 20 }} />
                                }
                            }} />
                    </View>
                    <View style={{ flex: 1, marginTop: 20 }} >
                        <View style={{ flex: .4 }} >
                            <InfoLine
                                onPress={() => navigate('Info', {
                                    header: 'Reviews',
                                    reviews: restaurant.reviews
                                } as ListPageInfoType)}
                                title='Review & Ratings'
                                info={`See all (${restaurant.reviews.length})`}
                                style={{ marginHorizontal: 20 }} />
                        </View>
                        <View style={{ flex: 2, borderColor: 'green' }} >
                            <FlatList
                                data={restaurant.reviews.filter((_, i) => i < 3)}
                                showsHorizontalScrollIndicator={false}
                                scrollEnabled={false}
                                keyExtractor={item => item.id}
                                renderItem={({ item }) => {
                                    return <ReviewUser
                                        key={item.id}
                                        review={item.text}
                                        rating={item.rating}
                                        name={item.name}
                                        avatar={item.avatar} />
                                }} />
                        </View>
                    </View>
                </View>
            </Animated.View>
            <TouchableOpacity
                activeOpacity={0.8}
                style={styles.btnRate}
                onPress={() => { navigate('ReviewAndRatings', { id: restaurant.id }) }}>
                <TextBold
                    text='Rate Your Experiance'
                    fontSize={20}
                    color={THEME.WHITE_COLOR} />
            </TouchableOpacity>
        </View>
    )
}


const styles = StyleSheet.create({
    content: {
        flex: 1,
        position: 'relative'
    },
    header: {
        position: 'absolute',
        height: Dimensions.get('window').height / 2,
        top: 0,
        right: 0,
        left: 0,
    },
    footer: {
        borderTopRightRadius: 20,
        borderTopLeftRadius: 20,
        backgroundColor: THEME.WHITE_COLOR,
        position: 'absolute',
        height: Dimensions.get('window').height / 1.3,
        bottom: -100,
        right: 0,
        left: 0
    },
    btnRate: {
        position: 'absolute',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        backgroundColor: THEME.BTN_BACKGROUND_COLOR,
        height: Dimensions.get('window').height / 10,
        bottom: 0,
        left: 0,
        right: 0,
        borderTopLeftRadius: 30,
        borderTopRightRadius: 30
    },
    headerWrapGradient: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        flexDirection: 'row',
        paddingTop: 30
    },
    headerBtnBack: {
        alignSelf: 'stretch',
        width: 50,
        justifyContent: 'center',
        alignItems: 'center'
    },
    headerWrapGradientTitle: {
        flex: 1,
        justifyContent: 'space-between',
        alignItems: 'flex-start',
        flexDirection: 'row',
    },
    headerWrapGradientTitleShared: {
        flex: 0.3,
        justifyContent: 'space-around',
        flexDirection: 'row',
        alignItems: 'center',
    },
    footerTitle: {
        flex: 1,
    },
    footerNKD: {
        flex: 2,
        flexDirection: 'row',
        borderColor: 'green',
    },
    footerRating: {
        flex: .3,
        borderColor: 'red',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-evenly'
    },
    menuPhoto: {
        width: 120,
        height: '100%',
        backgroundColor: '#902020',
        marginLeft: 20,
        borderRadius: 10
    },
})