import { LinearGradient } from 'expo-linear-gradient'
import React, { useContext } from 'react'
import { View, StyleSheet, TouchableOpacity } from 'react-native'
import { TextRegular } from '../../../components/UI/TextRegular'
import { FilterContext } from '../../../context/filter/filterContext'
import { categoryMokData } from '../../../mock/category'
import { THEME } from '../../../theme'

export type selectCategoryType = {
    name: string
    isSelected: boolean
}



export const Categories: React.FC = () => {
    const { categories, setCategories } = useContext(FilterContext)

    const onPress = (name: string) => {
        if (categories.includes(name)) {
            const array = categories.filter(c => c !== name)
            setCategories(array)
        } else {
            setCategories([...categories, name])
        }
    }

    return <View style={styles.container}>
        {
            categoryMokData.map(item => {
                const isSelected = categories.includes(item.name)
                return (
                    <TouchableOpacity
                        key={item.id}
                        activeOpacity={.7}
                        onPress={() => onPress(item.name)}
                        style={{
                            ...styles.category,
                            borderWidth: isSelected ? 0 : 1
                        }}>
                        {
                            isSelected
                                ? <LinearGradient
                                    start={{ x: 1, y: 1 }}
                                    end={{ x: 0, y: 1 }}
                                    colors={item.linerGradient}
                                    style={styles.gradient}
                                >
                                    <TextRegular text={item.name} color={THEME.WHITE_COLOR} />
                                </LinearGradient>
                                : <TextRegular text={item.name} color={THEME.TEXT_COLOR} />
                        }
                    </TouchableOpacity>
                )
            })
        }
    </View>
}

const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        alignItems: 'flex-start',
        flexWrap: 'wrap',
        flexDirection: 'row',
        flex: 0.7,
        alignSelf: 'stretch',
        paddingTop: 25,
        paddingHorizontal: 10,

    },
    category: {
        borderRadius: 8,
        height: 40,
        width: 80,
        marginRight: 10,
        marginBottom: 10,
        justifyContent: 'center',
        alignItems: 'center',
        borderWidth: 1,
        borderColor: THEME.TEXT_COLOR
    },
    gradient: {
        flex: 1,
        alignSelf: 'stretch',
        borderRadius: 8,
        justifyContent: 'center',
        alignItems: 'center'
    },
})