import React, { useContext, useEffect, useState } from 'react'
import {
    View,
    StyleSheet,
} from 'react-native'
import { PanGestureHandler, TextInput } from 'react-native-gesture-handler'
import Animated, { useAnimatedGestureHandler, useAnimatedProps, useAnimatedStyle, useDerivedValue, useSharedValue } from 'react-native-reanimated'
import { FilterContext } from '../../../context/filter/filterContext'
import { THEME } from '../../../theme'


const SLIDER_WIDTH = 250
const KNOB_WIDTH = 30
const MAX_RANGE = 100

interface ITextInput {
    text: Animated.SharedValue<string>
}

interface IDistance {
    getDistance: (distance: number) => void
}


export const Distance: React.FC<IDistance> = ({ getDistance }) => {
    const { distance } = useContext(FilterContext)
    const translateX = useSharedValue(distance)
    const isSliding = useSharedValue(false)

    const scrollTranslationStyle = useAnimatedStyle(() => {
        return { transform: [{ translateX: translateX.value }] }
    })

    const progressStyle = useAnimatedStyle(() => {
        return {
            width: translateX.value + KNOB_WIDTH,
        }
    })

    const AnimatedTextInput = Animated.createAnimatedComponent(TextInput)

    const AnimatedText = ({ text }: ITextInput) => {
        const animatedProps = useAnimatedProps(() => {
            return {
                text: text.value,
            }
        })

        return (
            <AnimatedTextInput
                underlineColorAndroid="transparent"
                editable={false}
                style={{ textAlign: 'center', color: THEME.WHITE_COLOR }}
                value={text.value}
                animatedProps={animatedProps}
            />
        )
    }

    const clamp = (value: number, lowerBound: number, upperBound: number) => {
        'worklet'
        return Math.min(Math.max(lowerBound, value), upperBound)
    }

    const culcDistance = (translateX: number) => {
        'worklet'
        const sliderRange = SLIDER_WIDTH - KNOB_WIDTH
        const oneStepValue = sliderRange / MAX_RANGE
        const distance = Math.ceil(translateX / oneStepValue)
        return String(distance)
    }

    const stepText = useDerivedValue(() => culcDistance(translateX.value))

    const onGestureEvent = useAnimatedGestureHandler({
        onStart: (_, ctx: { offsetX: number }) => {
            ctx.offsetX = translateX.value
        },
        onActive: (event, ctx) => {
            isSliding.value = true
            translateX.value = clamp(
                event.translationX + ctx.offsetX,
                0,
                SLIDER_WIDTH - KNOB_WIDTH
            )
        },
        onEnd: (event, ctx) => {
            isSliding.value = false
            getDistance(+culcDistance(clamp(
                event.translationX + ctx.offsetX,
                0,
                SLIDER_WIDTH - KNOB_WIDTH
            )))
        }
    })

    return <View style={styles.slider}>
        <Animated.View style={[styles.progress, progressStyle]} />
        <PanGestureHandler onGestureEvent={onGestureEvent}>
            <Animated.View style={[styles.knob, scrollTranslationStyle]} >
                <View pointerEvents='none'>
                    <AnimatedText text={stepText} />
                </View>
            </Animated.View>
        </PanGestureHandler>
    </View>
}

const styles = StyleSheet.create({
    slider: {
        height: KNOB_WIDTH,
        width: SLIDER_WIDTH,
        borderRadius: KNOB_WIDTH / 2,
        justifyContent: 'center',
        backgroundColor: '#cfcfcf',
        elevation: 5
    },
    progress: {
        ...StyleSheet.absoluteFillObject,
        borderRadius: KNOB_WIDTH / 2,
        backgroundColor: THEME.BTN_BACKGROUND_COLOR
    },
    knob: {
        height: KNOB_WIDTH,
        width: KNOB_WIDTH,
        backgroundColor: THEME.BTN_BACKGROUND_COLOR,
        borderRadius: KNOB_WIDTH / 2,
        elevation: 10,
        justifyContent: 'center',
        alignItems: 'center'
    },

})