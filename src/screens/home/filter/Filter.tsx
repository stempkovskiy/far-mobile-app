import React, { useContext, useRef } from 'react'
import { StyleSheet, View } from 'react-native'
import { RatingStar } from '../../../components/UI/RatingStar'
import { TextRegular } from '../../../components/UI/TextRegular'
import { THEME } from '../../../theme'
import { Categories } from './Сategories'
import { Distance } from './Distance'
import { FilterButton } from '../../../components/UI/FilterButton'
import { FilterContext, initStarsFilter, Rating, Star } from '../../../context/filter/filterContext'
import { Toast } from '../../../components/UI/Toast'
import { AppContext } from '../../../context/app/appContenxt'


export const Filter: React.FC = () => {
    const { setRating } = useContext(FilterContext)
    const distanseRef = useRef(0)
    const { initToast, toast } = useContext(AppContext)
    const getRating = (rating: Rating) =>
        setRating(rating.stars, rating.value)

    const getDistance = (distance: number) => distanseRef.current = distance

    return (
        <View style={styles.container}>
            <Toast onClick={() => initToast({ ...toast, hide: false })} />
            <View style={styles.title}>
                <TextRegular text='Select Category' fontSize={20} />
            </View>
            <Categories />
            <View style={{ flex: 0.5, alignSelf: 'stretch', alignItems: 'center' }} >
                <View style={{ ...styles.title, marginBottom: 10 }}>
                    <TextRegular text='Distance' fontSize={20} />
                </View>
                <Distance getDistance={getDistance} />
            </View>
            <View style={{ flex: 0.5, alignSelf: 'stretch', alignItems: 'center' }} >
                <View style={{ ...styles.title, marginBottom: 10 }}>
                    <TextRegular text='Rating' fontSize={20} />
                </View>
                <RatingStar
                    getRating={getRating}
                    isFilterStars={true} />
            </View>
            <FilterButton
                distance={distanseRef.current} />
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: THEME.WHITE_COLOR,
        flex: 1,
        alignItems: 'center'
    },
    title: {
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'stretch',
    }
})