import { useNavigation } from '@react-navigation/native'
import React, { useContext } from 'react'
import { ImageBackground, View, StyleSheet } from 'react-native'
import { getAllUnFollowUsersAsync } from '../../api/user'
import { InfoLine } from '../../components/InfoLine'
import { ListPageInfoType } from '../../components/ListPageInfo'
import { AppContext } from '../../context/app/appContenxt'

export const Friends: React.FC = () => {
    const { userInfo } = useContext(AppContext)
    const { navigate } = useNavigation()


    const seeAll = async () => {
        const response = await getAllUnFollowUsersAsync(userInfo.id)
        if (response.success) {
            navigate('Info', {
                users: response.followings,
                header: 'Users'
            } as ListPageInfoType)
        }
    }

    return <>
        <InfoLine
            onPress={seeAll}
            title='Friends'
            info={`See all (${userInfo.following.length})`}
        />
        <View style={styles.friends}>
            {
                userInfo.following
                    .slice(0, 6)
                    .map(item => {
                        return <ImageBackground
                            key={item.id}
                            borderRadius={30}
                            source={{ uri: item.avatar }}
                            style={styles.friend} />
                    })
            }
        </View>
    </>
}

const styles = StyleSheet.create({
    friends: {
        marginVertical: 10,
        alignSelf: 'stretch',
        flexDirection: 'row'
    },
    friend: {
        width: 50,
        height: 50,
        marginRight: 3
    }
})