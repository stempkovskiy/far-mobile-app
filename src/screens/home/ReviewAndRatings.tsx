import { useRoute } from '@react-navigation/native'
import React, { useContext, useState } from 'react'
import {
    View,
    StyleSheet,
    Dimensions,
    ScrollView,
    TextInput,
    KeyboardAvoidingView,
    Platform,
    TouchableOpacity
} from 'react-native'
import { addReviewAsync, getUserAsync, GetUserEnum, NewReviewRequest } from '../../api/user'
import { RatingStar } from '../../components/UI/RatingStar'
import { TextBold } from '../../components/UI/TextBold'
import { TextRegular } from '../../components/UI/TextRegular'
import { Toast, ToastTypeEnum } from '../../components/UI/Toast'
import { AppContext } from '../../context/app/appContenxt'
import { AuthContext } from '../../context/auth/authContext'
import { FilterContext } from '../../context/filter/filterContext'
import { THEME } from '../../theme'

type ReviweAndRatingParams = {
    id: string
}

export const ReviweAndRating: React.FC = () => {

    const [review, setReview] = useState('')
    const { toast, initToast } = useContext(AppContext)
    const { userInfo, updateUserInfo } = useContext(AppContext)
    const [rating, setRating] = useState(0)
    const { id } = useRoute().params as ReviweAndRatingParams


    const addReview = async () => {
        const body: NewReviewRequest = {
            IdRestauran: id,
            IdUser: userInfo.id,
            Rating: rating,
            Text: review
        }
        if (body.Rating && body.Text) {
            const response = await addReviewAsync(body)
            if (response.success) {
                const user = await getUserAsync(userInfo.id, GetUserEnum.id)
                if (user.success) {
                    updateUserInfo(user.data?.userInfos[0]!)
                    initToast({ hide: true, type: ToastTypeEnum.success, text: 'Your review was added' })
                }
            } else {
                initToast({ hide: true, type: ToastTypeEnum.error, text: response.message![0] })
            }
        }
    }

    return <KeyboardAvoidingView
        style={{ flex: 1 }}
        behavior={Platform.OS == "ios" ? 'padding' : 'height'}
        enabled={Platform.OS === 'ios'}
    >
        <Toast onClick={() => initToast({ ...toast, hide: false })} />
        <ScrollView
            showsVerticalScrollIndicator={false}
            style={styles.scroll}
        >
            <Toast onClick={() => initToast({ ...toast, hide: false })} />
            <RatingStar
                getRating={r => setRating(r.value)}
                isFilterStars={false} />
            <View style={styles.contentText}>
                <TextRegular
                    text='Rate your experience'
                    color={THEME.TEXT_COLOR} />
            </View>
            <TextInput
                multiline={true}
                onChangeText={setReview}
                placeholder='Write your experience'
                style={styles.contenTextInput} />
        </ScrollView>
        <TouchableOpacity
            onPress={addReview}
            style={styles.footer} >
            <TextBold text='Done' fontSize={20} color={THEME.WHITE_COLOR} />
        </TouchableOpacity>
    </KeyboardAvoidingView>
}

const styles = StyleSheet.create({
    container: {
        position: 'relative',
        flex: 1
    },
    scroll: {
        flex: 1,
        backgroundColor: THEME.WHITE_COLOR,
        paddingHorizontal: 20,
        position: 'relative'
    },
    content: {
        flex: 1,
        paddingHorizontal: 20,
        width: Dimensions.get('window').width
    },
    contentText: {
        marginTop: 5,
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center',
    },
    contenTextInput: {
        marginTop: 10,
        padding: 20,
        borderRadius: 20,
        borderWidth: 1,
        borderColor: THEME.TEXT_COLOR,
        textAlignVertical: 'top',
        fontFamily: THEME.JOSEFINSANS_REGULAR,
        width: '100%',
        height: Dimensions.get('window').height / 4
    },
    footer: {
        bottom: 0,
        left: 0,
        right: 0,
        height: Dimensions.get('window').height / 10,
        backgroundColor: THEME.BTN_BACKGROUND_COLOR,
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
    }
})