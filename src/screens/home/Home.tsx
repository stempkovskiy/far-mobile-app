import React, { useContext, useEffect, useState } from 'react'
import { View, StyleSheet, SafeAreaView, Dimensions, ImageBackground, ScrollView, StatusBar } from 'react-native'
import { useNavigation } from '@react-navigation/native'
import { THEME } from '../../theme'
import { TextBold } from '../../components/UI/TextBold'
import { CardRestaurans, } from '../../components/CardRestaurans'
import { FlatList, TouchableOpacity } from 'react-native-gesture-handler'
import { LinearGradient } from 'expo-linear-gradient'
import { categoryMokData } from '../../mock/category'
import { InfoLine } from '../../components/InfoLine'
import { AppContext } from '../../context/app/appContenxt'
import { Friends } from './Friends'
import { ITrendingRestauransRouteData } from './TrendingRestaurant'
import * as Location from 'expo-location'

export type WidthElemet = {
    searchWidth: number,
    widthOtherElement: number
}
export const Home: React.FC = () => {
    const { navigate } = useNavigation()
    const { restaurants, searchAtHomeRestaursnts } = useContext(AppContext)

    useEffect(() => {
        Location.getPermissionsAsync()
            .then(response => {
                if (response.status !== Location.PermissionStatus.GRANTED) {
                    navigate('PermissionGPS')
                }
            })
    }, [])

    return (
        <>
            <StatusBar hidden={true} />
            {
                !searchAtHomeRestaursnts.length
                    ? <ScrollView showsVerticalScrollIndicator={false} contentContainerStyle={styles.scroll}>
                        {
                            <SafeAreaView style={styles.container}>
                                <InfoLine
                                    title='Trending Restaurants'
                                    info={`See all (${restaurants.length})`}
                                    onPress={() => navigate('Trending', { restaurants } as ITrendingRestauransRouteData)}
                                />
                                <View style={styles.restaurants}>
                                    <FlatList
                                        showsHorizontalScrollIndicator={false}
                                        horizontal={true}
                                        data={restaurants.filter(item => item).slice(0, 3)}
                                        keyExtractor={item => `${item.id!}`}
                                        renderItem={({ item }) => <CardRestaurans info={item} />
                                        }
                                    />
                                </View>


                                <InfoLine
                                    title='Category'
                                    info={`See all (${categoryMokData.length})`}
                                    onPress={() => navigate('Categories')}
                                />
                                <View style={styles.categories}>
                                    {
                                        categoryMokData
                                            .slice(0, 3)
                                            .map((item) => {
                                                return (
                                                    <TouchableOpacity
                                                        onPress={() => navigate('Category', item)}
                                                        activeOpacity={.8}
                                                        key={item.id}>
                                                        <ImageBackground
                                                            borderRadius={7}
                                                            source={categoryMokData[+item.id].img}
                                                            style={styles.category}>
                                                            <LinearGradient
                                                                style={styles.categoryGradient}
                                                                colors={item.linerGradient}>
                                                                <TextBold
                                                                    text={item.name}
                                                                    fontSize={18}
                                                                    color={THEME.WHITE_COLOR} />
                                                            </LinearGradient>
                                                        </ImageBackground>
                                                    </TouchableOpacity>
                                                )
                                            })
                                    }
                                </View>

                                <Friends />

                            </SafeAreaView>
                        }
                    </ScrollView>
                    : <FlatList
                        contentContainerStyle={{ alignItems: 'center', flex: 1, backgroundColor: THEME.WHITE_COLOR }}
                        style={{ alignSelf: 'stretch' }}
                        data={searchAtHomeRestaursnts}
                        showsVerticalScrollIndicator={false}
                        keyExtractor={({ id }) => id}
                        renderItem={({ item }) =>
                            <CardRestaurans
                                style={{ margin: 0, width: Dimensions.get('window').width - 10 * 2 }}
                                info={{ ...item }} />}
                    />
            }
        </>
    )
}

const styles = StyleSheet.create({
    scroll: {
        flexGrow: 1,
        justifyContent: 'center',
        paddingBottom: 40
    },
    container: {
        alignItems: 'center',
        paddingVertical: 30,
        paddingHorizontal: 10,
        backgroundColor: THEME.WHITE_COLOR,
        flex: 1
    },
    restaurants: {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height / 2.5,
    },
    categories: {
        marginVertical: 10,
        alignSelf: 'stretch',
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    category: {
        width: 105,
        height: 105,
        borderRadius: 7,
        opacity: .9,
    },
    categoryGradient: {
        flex: 1,
        alignSelf: 'stretch',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 7
    },
});

