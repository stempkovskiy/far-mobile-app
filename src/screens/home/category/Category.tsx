import React, { useContext, useState } from 'react'
import { ImageBackground, StyleSheet, View, TouchableOpacity, FlatList, Dimensions, GestureResponderEvent } from 'react-native'
import { THEME } from '../../../theme'
import { LinearGradient } from 'expo-linear-gradient'
import { useNavigation, useRoute } from '@react-navigation/native'
import { Category as Data, categoryMokData } from '../../../mock/category'
import { TextRegular } from '../../../components/UI/TextRegular'
import { Ionicons } from '@expo/vector-icons'
import { CardRestaurans } from '../../../components/CardRestaurans'
import { AppContext } from '../../../context/app/appContenxt'
import Animated, { cos, useAnimatedGestureHandler, useAnimatedProps, useSharedValue } from 'react-native-reanimated'
import { PanGestureHandler, TextInput } from 'react-native-gesture-handler'

const centerWidth = Dimensions.get('window').width / 2

export const Category: React.FC = () => {
    const param = useRoute().params as Data
    const [data, setData] = useState(param)

    const { restaurants } = useContext(AppContext)
    const { goBack } = useNavigation()

    const onGestureEvent = useAnimatedGestureHandler({
        onEnd: (event, _) => {
            if (event.translationX < 0) {
                categoryMokData
                    .forEach(c => {
                        if (+data.id + 1 === +c.id) {
                            setData(c)
                        }
                    })
            } else {
                categoryMokData
                    .forEach(c => {
                        if (+data.id - 1 === +c.id) {
                            setData(c)
                        }
                    })
            }
        }
    })

    return <View style={styles.container}>
        <PanGestureHandler onGestureEvent={onGestureEvent}>
            <Animated.View style={styles.header}>
                <ImageBackground source={data.img} style={styles.header}>
                    <LinearGradient style={styles.gradient} colors={data.linerGradient} >
                        <TouchableOpacity
                            onPress={goBack}
                            style={styles.btnBack}
                            activeOpacity={.8}>
                            <Ionicons name='ios-arrow-back' size={30} color={THEME.WHITE_COLOR} />
                        </TouchableOpacity>
                        <View style={styles.title}>
                            <TextRegular text={data.name} fontSize={25} color={THEME.WHITE_COLOR} />
                        </View>
                        <View style={styles.lineWrap}>
                            {
                                categoryMokData.map((_, i) => {
                                    return <View key={i}
                                        style={{
                                            ...styles.line,
                                            opacity: +data.id !== i ? .5 : 1
                                        }} />
                                })
                            }
                        </View>
                    </LinearGradient>
                </ImageBackground>
            </Animated.View>
        </PanGestureHandler>

        <View style={styles.content} >
            <FlatList
                contentContainerStyle={{ alignItems: 'center', flex: 1 }}
                style={{ alignSelf: 'stretch' }}
                data={restaurants.filter(item => item.kitchen === data.name)}
                showsVerticalScrollIndicator={false}
                keyExtractor={item => item.id}
                renderItem={({ item }) =>
                    <CardRestaurans
                        style={{
                            margin: 0,
                            width: Dimensions.get('window').width - 10 * 2
                        }}
                        info={{ ...item }} />}
            />
        </View>
    </View>
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: THEME.WHITE_COLOR
    },
    header: {
        flex: 2.5
    },
    content: {
        flex: 10,
    },
    gradient: {
        justifyContent: 'flex-end',
        alignItems: 'center',
        alignSelf: 'stretch',
        flex: 1,
        opacity: .8,
    },
    lineWrap: {
        justifyContent: 'space-between',
        paddingVertical: 10,
        width: '50%',
        flexDirection: 'row',
    },
    line: {
        width: 15,
        height: 3,
        backgroundColor: THEME.WHITE_COLOR,
        borderRadius: 30,
    },
    title: {
        paddingTop: 15,
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    btnBack: {
        position: 'absolute',
        left: 0,
        top: 0,
        bottom: 0,
        justifyContent: 'center',
        alignItems: 'center',
        width: '20%'
    }
})