import React from 'react'
import { useNavigation } from '@react-navigation/native'
import { FlatList } from 'react-native-gesture-handler'
import { LinearGradient } from 'expo-linear-gradient'
import { Dimensions, ImageBackground, StyleSheet, TouchableOpacity, View } from 'react-native'
import { TextRegular } from '../../../components/UI/TextRegular'
import { categoryMokData } from '../../../mock/category'
import { THEME } from '../../../theme'

export const Categories: React.FC = () => {
    const navigation = useNavigation()
    return <View style={styles.container}>
        <FlatList
            showsVerticalScrollIndicator={false}
            style={{ alignSelf: 'stretch' }}
            data={categoryMokData}
            keyExtractor={(item) => item.id}
            renderItem={({ item }) => {
                return <TouchableOpacity
                    onPress={() => navigation.navigate('Category', item)}
                    activeOpacity={.8}
                    style={styles.category} >
                    <ImageBackground
                        borderRadius={10}
                        source={item.img} style={{ flex: 1, alignSelf: 'stretch' }}>
                        <LinearGradient
                            start={{ x: 1, y: 1 }}
                            end={{ x: 0, y: 1 }}
                            colors={item.linerGradient}
                            style={styles.gradient} >
                            <TextRegular text={item.name} fontSize={30} color={THEME.WHITE_COLOR} />
                            <View style={styles.line} />
                        </LinearGradient>
                    </ImageBackground>
                </TouchableOpacity>
            }} />
    </View>
}
const styles = StyleSheet.create({
    container: {
        backgroundColor: THEME.WHITE_COLOR,
        paddingHorizontal: 20,
        flex: 1,
        alignItems: 'center'
    },
    category: {
        elevation: 5,
        borderRadius: 10,
        width: '100%',
        height: Dimensions.get('window').height / 6,
        marginBottom: 20,
    },
    gradient: {
        position: 'relative',
        alignSelf: 'stretch',
        flex: 1,
        borderRadius: 10,
        opacity: .8,
        justifyContent: 'center',
        alignItems: 'center'
    },
    line: {
        position: 'absolute',
        right: 15,
        height: '40%',
        width: 4,
        borderRadius: 10,
        backgroundColor: THEME.WHITE_COLOR,
        opacity: .5
    }
}) 