import { useNavigation } from '@react-navigation/native'
import React, { useContext, useState } from 'react'
import { View, StyleSheet, ImageBackground, Dimensions, TouchableOpacity } from 'react-native'
import { ListPageInfoType } from '../../components/ListPageInfo'
import { TextRegular } from '../../components/UI/TextRegular'
import { AppContext } from '../../context/app/appContenxt'
import { AuthContext } from '../../context/auth/authContext'
import { THEME } from '../../theme'

export const Profile: React.FC = () => {
    const { userInfo } = useContext(AppContext)
    const { signOut } = useContext(AuthContext)
    const { navigate } = useNavigation()
    const [btn, setBtn] = useState(false)
    const listInfoPage = (header: string) =>
        navigate('Info', { header } as ListPageInfoType)
    return <View style={styles.container}>
        <ImageBackground
            source={{ uri: userInfo.avatar.path }}
            borderRadius={50}
            style={styles.avatar} />
        <View style={styles.info}>
            <TextRegular text={userInfo.login} fontSize={20} color='#3E3F68' />
            <TextRegular text={userInfo.email} fontSize={13} color={THEME.TEXT_COLOR} />
        </View>

        <View style={styles.socialInfo}>
            <TouchableOpacity
                onPress={() => listInfoPage('Reviews')}
                style={styles.block}>
                <TextRegular text={`${userInfo.reviews.length}`} color={THEME.BTN_BACKGROUND_COLOR} />
                <TextRegular text='Reviews' color={THEME.TEXT_COLOR} fontSize={13} />
            </TouchableOpacity>
            <View style={styles.line} />
            <TouchableOpacity
                onPress={() => listInfoPage('Followers')}
                style={styles.block}>
                <TextRegular text={`${userInfo.followers.length}`} color={THEME.BTN_BACKGROUND_COLOR} />
                <TextRegular text='Followers' color={THEME.TEXT_COLOR} fontSize={13} />
            </TouchableOpacity>
            <View style={styles.line} />
            <TouchableOpacity
                onPress={() => listInfoPage('Followings')}
                style={styles.block}>
                <TextRegular text={`${userInfo.following.length}`} color={THEME.BTN_BACKGROUND_COLOR} />
                <TextRegular text='Following' color={THEME.TEXT_COLOR} fontSize={13} />
            </TouchableOpacity>
        </View>

        <View style={styles.btns}>
            <TouchableOpacity
                onPress={() => setBtn(false)}
                style={{
                    ...styles.btn,
                    borderWidth: !btn ? 0 : 1,
                    backgroundColor: !btn
                        ? THEME.BTN_BACKGROUND_COLOR
                        : THEME.WHITE_COLOR
                }}>
                <TextRegular text='Edit Profile' color={!btn ? THEME.WHITE_COLOR : THEME.TEXT_COLOR} />
            </TouchableOpacity>

            <TouchableOpacity
                onPress={signOut}
                style={{
                    ...styles.btn,
                    borderWidth: btn ? 0 : 1,
                    backgroundColor: btn
                        ? THEME.BTN_BACKGROUND_COLOR
                        : THEME.WHITE_COLOR
                }}>
                <TextRegular text='Settings' color={btn ? THEME.WHITE_COLOR : THEME.TEXT_COLOR} />
            </TouchableOpacity>
        </View>
    </View>
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: THEME.WHITE_COLOR,
        flex: 1,
        alignItems: 'center',
        paddingHorizontal: 20
    },
    avatar: {
        width: 100,
        height: 100
    },
    info: {
        marginTop: 10,
        alignItems: 'center'
    },
    socialInfo: {
        paddingVertical: 10,
        marginTop: 10,
        height: Dimensions.get('window').height / 9,
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'space-evenly',
        alignItems: 'center',
    },
    block: {
        alignSelf: 'stretch',
        alignItems: 'center',
        justifyContent: 'space-evenly'
    },
    line: {
        opacity: .6,
        width: 1,
        height: '100%',
        backgroundColor: '#8A98BA',
        marginLeft: 5,
        marginRight: 5
    },
    btns: {
        justifyContent: 'space-evenly',
        flexDirection: 'row',
        width: '100%',
        height: Dimensions.get('window').height / 9,
        padding: 10,

    },
    btn: {
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 5,
        borderColor: THEME.TEXT_COLOR,
        width: '40%',
        borderWidth: 1,
    }
})

