import React, { useContext, useState } from "react"
import { View, StyleSheet, ImageBackground, Keyboard, TouchableOpacity, Image, ScrollView, Text, SafeAreaView, Dimensions } from "react-native"
import * as ImagePicker from 'expo-image-picker'
import { TextRegular } from "../components/UI/TextRegular"
import { Input, InputIcon } from "../components/UI/Input"
import { Button } from "../components/UI/Button"
import { SignInProps } from "../navigation/types"
import { THEME } from "../theme"
import { Toast, ToastTypeEnum } from "../components/UI/Toast"
import { confirmPasswordValidator, emailValidator, isEmptyFields, loginValidator, passwordValidator } from "../validation/validations"
import { IUserRegistrationRequest } from "../api/authorization"
import { AuthContext } from "../context/auth/authContext"
import { AppContext } from "../context/app/appContenxt"
import { AntDesign } from "@expo/vector-icons"
import Constants from 'expo-constants';
import { Distance } from "./home/filter/Distance"

type RegistationInitType = {
  avatar: ImageInfo,
  login: string,
  email: string,
  password: string,
  confirmPassword: string
}

export interface ImageInfo {
  uri: string,
  name: string,
  type: string
}

export const Registarion: React.FC<SignInProps> = ({ navigation }) => {
  const { registration } = useContext(AuthContext)
  const { loading, initToast, toast } = useContext(AppContext)
  const [uri, setUri] = useState('')
  const [data, setData] = useState<RegistationInitType>({
    avatar: {
      name: '',
      type: '',
      uri: ''
    },
    login: '',
    email: '',
    password: '',
    confirmPassword: ''
  })

  const register = async () => {
    const body: IUserRegistrationRequest = {
      Avatar: data.avatar,
      Name: data.login,
      Email: data.email,
      Password: data.password
    }
    if (isEmptyFields([body.Name, body.Email, body.Password])) {
      Keyboard.dismiss()
      console.log(body)
      const response = await registration(body)
      if (response.length) {
        initToast({ hide: true, type: ToastTypeEnum.error, text: response[0] })
      } else {
        initToast({ hide: true, type: ToastTypeEnum.success, text: 'Registration complited!' })
      }
    } else {
      Keyboard.dismiss()
      initToast({ hide: true, type: ToastTypeEnum.error, text: 'All fields must be filled!' })
    }
  }


  const loadImage = async () => {
    const response = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.All,
      allowsEditing: true,
      aspect: [4, 3],
      quality: 1
    })
    if (!response.cancelled) {
      setUri(response.uri)
      setData({
        ...data, avatar: {
          name: response.uri.split('/').pop()!,
          type: 'image/jpg',
          uri: response.uri
        }
      })
    }
  }
  return (
    <ScrollView
      contentContainerStyle={{ height: Dimensions.get('window').height }}>
      <ImageBackground
        blurRadius={3}
        source={require('../../assets/img/background/registaration.png')}
        style={styles.container}>
        <View style={styles.child} />

        <Toast
          top={50}
          onClick={() => initToast({ ...toast, hide: false })}
        />


        <View style={styles.avatar}>
          <View style={styles.bgAvatar} >
            {
              uri
                ? <Image source={{ uri }} style={{ width: '100%', height: '100%' }} />
                : <View style={{
                  backgroundColor: THEME.WHITE_COLOR,
                  opacity: .3,
                  width: '100%',
                  height: '100%',
                  justifyContent: 'center',
                  alignItems: 'center'
                }}>
                  <AntDesign name='user' size={40} color={THEME.WHITE_COLOR} />
                </View>
            }
          </View>
          <TouchableOpacity
            onPress={loadImage}
            style={styles.btnAvatar} >
            <AntDesign name='arrowup' size={30} color={THEME.WHITE_COLOR} />
          </TouchableOpacity>
        </View>


        <Input
          icon={InputIcon.user}
          placeholder="Name"
          secureTextEntry={false}
          validatorsFunc={[loginValidator]}
          placeholderTextColor={THEME.WHITE_COLOR}
          colorIcon={THEME.WHITE_COLOR}
          onChange={login => setData({ ...data, login })}
        />
        <Input
          icon={InputIcon.email}
          placeholder="Email"
          secureTextEntry={false}
          validatorsFunc={[emailValidator]}
          placeholderTextColor={THEME.WHITE_COLOR}
          colorIcon={THEME.WHITE_COLOR}
          onChange={email => setData({ ...data, email })}
        />
        <Input
          icon={InputIcon.lockClose}
          placeholder="Password"
          secureTextEntry={true}
          validatorsFunc={[passwordValidator]}
          placeholderTextColor={THEME.WHITE_COLOR}
          colorIcon={THEME.WHITE_COLOR}
          onChange={password => setData({ ...data, password })}
        />
        <Input
          icon={InputIcon.lockClose}
          placeholder="Confirm Password"
          secureTextEntry={true}
          validatorsFunc={[confirmPasswordValidator]}
          placeholderTextColor={THEME.WHITE_COLOR}
          colorIcon={THEME.WHITE_COLOR}
          password={data.password}
          onChange={confirmPassword => setData({ ...data, confirmPassword })}
        />


        <Button
          text='Register now'
          onPress={register}
          loading={loading} />

        <View style={styles.login}>
          <TextRegular text='Already have an account?' color={THEME.WHITE_COLOR} />
          <TouchableOpacity onPress={() => navigation.pop()}>
            <TextRegular text='Login' color='#5663FF' />
          </TouchableOpacity>
        </View>
      </ImageBackground>
    </ScrollView>
  )
}

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 30,
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    resizeMode: 'contain',
    position: 'relative',
  },
  login: {
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
  },
  child: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    backgroundColor: 'rgba(0,0,0,0.3)'
  },
  avatar: {
    width: '100%',
    height: 200,
    justifyContent: 'center',
    alignItems: 'center'
  },
  bgAvatar: {
    overflow: 'hidden',
    borderRadius: 100,
    width: '45%',
    height: '70%',
    justifyContent: 'center',
    alignItems: 'center'
  },
  btnAvatar: {
    backgroundColor: THEME.BTN_BACKGROUND_COLOR,
    borderColor: THEME.WHITE_COLOR,
    borderWidth: 2,
    borderRadius: 30,
    position: 'absolute',
    height: 50,
    width: 50,
    bottom: 20,
    left: '55%',
    justifyContent: 'center',
    alignItems: 'center'
  }
})
