import React, { useContext } from 'react'
import { View, StyleSheet, TouchableOpacity, Image } from 'react-native'
import { FlatList } from 'react-native-gesture-handler'
import { CardRestaurans } from '../../components/CardRestaurans'
import { AppContext } from '../../context/app/appContenxt'
import { THEME } from '../../theme'

export const Favourite: React.FC = () => {
    const { restaurants, removeFavourite } = useContext(AppContext)
    const lengthFavourite = restaurants.filter((item) => item.isFavouvarit).length - 1
    return <View style={styles.container}>
        <View style={styles.content}>
            <FlatList
                style={{ width: '100%', }}
                data={restaurants.filter((item) => item.isFavouvarit)}
                showsVerticalScrollIndicator={false}
                keyExtractor={item => item.id!.toString()}
                renderItem={({ item, index }) =>
                    <>
                        <CardRestaurans
                            info={{
                                ...item,
                                style: { ...styles.card, marginBottom: index !== lengthFavourite ? 10 : 40 }
                            }} />
                        <TouchableOpacity
                            activeOpacity={0.8}
                            style={styles.icon}
                            onPress={() => removeFavourite(item)}>
                            <Image
                                source={THEME.FAVOURITE_ACTIVE_IMG}
                                width={20}
                                height={20}
                                resizeMode='center' />
                        </TouchableOpacity>
                    </>
                }
            />
        </View>
    </View>
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: THEME.WHITE_COLOR,
        justifyContent: 'center',
        alignItems: 'center',
        paddingBottom: 50,
    },
    content: {
        alignSelf: 'stretch',
        flex: 1,
        paddingHorizontal: 20,
    },
    card: {
        marginLeft: 0,
        marginTop: 0,
        width: '100%'
    },
    icon: {
        elevation: 10,
        position: 'absolute',
        top: 120,
        right: 10,
        height: 50,
        width: 50,
        borderRadius: 50,
        backgroundColor: THEME.WHITE_COLOR,
        justifyContent: 'center',
        alignItems: 'center',
    },
})

