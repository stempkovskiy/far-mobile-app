import React from 'react'
import { View, StyleSheet } from 'react-native'
import { TextBold } from '../../components/UI/TextBold'

export const Notification: React.FC = () => {
    return <View style={styles.container}>
        <TextBold text='Notification page' color='black' />
    </View>
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
    }
})

