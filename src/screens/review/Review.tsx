import { AntDesign } from '@expo/vector-icons'
import { useNavigation } from '@react-navigation/native'
import React, { useContext, useState } from 'react'
import { View, StyleSheet, TextInput, ScrollView } from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler'
import { Restauran } from '../../api/restauran'
import { addReviewAsync } from '../../api/user'
import { CardRestaurans } from '../../components/CardRestaurans'
import { ReviewHeader } from '../../components/UI/headers/ReviewHeader'
import { RatingStar } from '../../components/UI/RatingStar'
import { Search } from '../../components/UI/Search'
import { TextBold } from '../../components/UI/TextBold'
import { TextRegular } from '../../components/UI/TextRegular'
import { AppContext } from '../../context/app/appContenxt'
import { THEME } from '../../theme'

export const Review: React.FC = () => {
    const { restaurants, userInfo } = useContext(AppContext)
    const [rating, setRating] = useState(0)
    const [text, setText] = useState('')
    const [error, setError] = useState('')
    const [color, setColor] = useState('#fc0313');
    const [idSelected, setIdSelected] = useState<string[]>([])
    const { goBack } = useNavigation()
    const [data, setData] = useState<Restauran[]>([])

    const onChange = (text: string) => {
        if (text.trim()) {
            setData(restaurants.filter(r =>
                r.name
                    .toLowerCase()
                    .includes(text.toLowerCase())))
        } else {
            setData([])
        }
    }

    const onBtnPlus = (id: string) => setIdSelected([id])

    const post = async () => {
        if (data.length) {
            if (idSelected.length) {
                if (text && rating !== 0) {
                    console.log(`text: ${text}\nrating: ${rating}\nselected restaurans: ${idSelected}`)
                    idSelected.forEach(async id => {
                        const response = await addReviewAsync({
                            IdRestauran: id,
                            IdUser: userInfo.id,
                            Rating: rating,
                            Text: text
                        })
                        if (response.success) {
                            setColor('#42f545')
                            setError('Your review(s) has been successfully posted!')
                        } else {
                            setColor('#fc0313')
                            setError(response.message![0])
                        }
                    })
                } else {
                    setColor('#fc0313')
                    setError('Choose rating or write text!')
                }
            } else {
                setColor('#fc0313')
                setError('Choose restauran!')
            }
        } else {
            setColor('#fc0313')
            setError('Find restaurans!')
        }
    }
    return <View style={styles.container}>
        <ReviewHeader
            headerRight={post}
            headerLeft={goBack} />
        <ScrollView
            contentContainerStyle={{
                alignItems: 'center',
                paddingBottom: 90,
            }}
            showsVerticalScrollIndicator={false}
            style={styles.content} >
            {
                error
                    ? <View style={{ ...styles.error, backgroundColor: color }}>
                        <TextRegular text={error} color={THEME.WHITE_COLOR} />
                        <TouchableOpacity onPress={() => setError('')}>
                            <AntDesign name='closecircleo' size={20} color='#fff' />
                        </TouchableOpacity>
                    </View>
                    : <></>

            }
            <Search
                onChange={onChange}
                placeholder='Search Restaourants'
                icon={false} />
            {
                data.map(item => {
                    return (
                        <CardRestaurans
                            isBtnPlus={true}
                            onBtnPlus={onBtnPlus}
                            key={item.id}
                            style={{ marginHorizontal: 0 }}
                            info={{ ...item }} />
                    )
                })
            }
            <View style={styles.textLine}>
                <TextBold text='Ratings' fontSize={18} />
            </View>
            <RatingStar
                getRating={rating => setRating(rating.value)}
                isFilterStars={false}
            />
            <View style={styles.textLine}>
                <TextRegular text='Rate your experiance' color={THEME.TEXT_COLOR} />
            </View>
            <View style={styles.textLine}>
                <TextBold text='Review' fontSize={18} />
            </View>
            <TextInput
                multiline={true}
                onChangeText={setText}
                placeholder='Write your experience'
                style={styles.contenTextInput} />
        </ScrollView>
    </View>
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: THEME.WHITE_COLOR,
        flex: 1,
    },
    content: {
        paddingHorizontal: 20,
        flex: 1,
    },
    textLine: {
        flex: 1,
        marginTop: 15,
        alignItems: 'center'
    },
    contenTextInput: {
        width: '100%',
        marginTop: 10,
        padding: 20,
        borderRadius: 20,
        borderWidth: 1,
        borderColor: THEME.TEXT_COLOR,
        textAlignVertical: 'top',
        fontFamily: THEME.JOSEFINSANS_REGULAR,
        height: 150
    },
    error: {
        width: '90%',
        paddingVertical: 10,
        paddingHorizontal: 5,
        borderRadius: 10,
        marginBottom: 5,
        flexDirection: 'row',
        justifyContent: 'space-between'
    }
})

