import React, { useState, useContext, useEffect } from "react"
import {
  View,
  StyleSheet,
  ImageBackground,
  TouchableOpacity,
  Text
} from "react-native"
import { Input, InputIcon } from "../components/UI/Input"
import { THEME } from "../theme"
import { TextBold } from "../components/UI/TextBold"
import { Button } from "../components/UI/Button"
import { passwordValidator, emailValidator, isEmptyFields, confirmPasswordValidator } from "../validation/validations"
import { TextRegular } from "../components/UI/TextRegular"
import { AuthContext } from '../context/auth/authContext'
import { useNavigation } from "@react-navigation/native"
import { AppContext } from "../context/app/appContenxt"
import { Toast, ToastTypeEnum } from "../components/UI/Toast"



export const SignIn: React.FC = () => {
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  const { signIn } = useContext(AuthContext)
  const { loading, toast, initToast } = useContext(AppContext)
  const navigations = useNavigation()

  const login = () => {
    if (isEmptyFields([password, email])) {
      signIn(email, password)
    } else {
      initToast({ hide: true, text: 'All fieald', type: ToastTypeEnum.error })
    }
  }


  return (
    <ImageBackground
      source={require("../../assets/img/background/signIn.png")}
      style={styless.container}
      blurRadius={3}
    >
      <Toast
        top={60}
        onClick={() => initToast({ ...toast, hide: false })} />

      <View style={styless.wraper}>
        <View style={styless.wraperForLogoAndInputs}>
          <View style={styless.logoText}>
            <TextBold text="FoodPost" fontSize={30} color={THEME.WHITE_COLOR} />
          </View>
          <View>
            <Input
              placeholder='Email'
              colorIcon={THEME.WHITE_COLOR}
              icon={InputIcon.email}
              placeholderTextColor={THEME.WHITE_COLOR}
              secureTextEntry={false}
              validatorsFunc={[emailValidator]}
              onChange={setEmail}
            />

            <Input
              placeholder='Password'
              colorIcon={THEME.WHITE_COLOR}
              placeholderTextColor={THEME.WHITE_COLOR}
              icon={InputIcon.lockClose}
              secureTextEntry={true}
              validatorsFunc={[passwordValidator]}
              onChange={setPassword}
            />
            {/* <TouchableOpacity style={styless.wraperForgotPass}>
              <TextBold text="Forgot Password?" color={THEME.WHITE_COLOR} />
            </TouchableOpacity> */}
          </View>
        </View>

        <Button
          text="Login"
          onPress={login}
          loading={loading} />

        <TouchableOpacity style={styless.bottomBorder} onPress={() => navigations.navigate('Registration')}>
          <TextRegular text="Create New Account" color={THEME.WHITE_COLOR} />
        </TouchableOpacity>

      </View>
    </ImageBackground>
  );
};

const styless = StyleSheet.create({
  container: {
    flex: 1,
    resizeMode: 'contain',
    position: 'relative',
  },
  wraper: {
    paddingHorizontal: 30,
    flex: 1,
    width: '100%',
    backgroundColor: 'rgba(0,0,0,.6)',
    alignItems: 'center',
    flexDirection: 'column',
  },
  wraperForLogoAndInputs: {
    display: "flex",
    justifyContent: "space-around",
    alignItems: "center",
    flex: 0.8,
  },
  logoText: {
    display: "flex",
    alignItems: "flex-start",
    fontSize: 35,
    fontFamily: THEME.JOSEFINSANS_BOILD,
    color: "white",
  },
  wraperForgotPass: {
    alignSelf: "flex-end",
  },
  bottomBorder: {
    marginTop: 10,
    borderBottomWidth: 1,
    borderColor: THEME.BORDER_BOTTOM_COLOR,
  },
  absolute: {
    position: "absolute",
    top: 0,
    left: 0,
    bottom: 0,
    right: 0
  }
});
