import React, { useContext } from 'react'
import { StyleSheet, View, Text, StatusBar, TouchableOpacity } from 'react-native'
import { Button } from '../components/UI/Button'
import { TextRegular } from '../components/UI/TextRegular'
import { THEME } from '../theme'
import * as Location from 'expo-location'
import { ImageHOC } from '../components/hoc/ImageHOC'
import { AppContext } from '../context/app/appContenxt'
import { useNavigation } from '@react-navigation/native'

const GRANDED = 'granted'

export const PremissionGPS: React.FC = () => {

    const { userInfo, setLocation } = useContext(AppContext)
    const {navigate} = useNavigation()
    const getPosition = async () => {
        const { status } = await Location.requestPermissionsAsync()
        if (status === GRANDED) {
            const { coords } = await Location.getCurrentPositionAsync()
            setLocation({latitude: coords.latitude, longitude: coords.longitude})
            navigate('Home')
        }
    }

    return <ImageHOC path={require('../../assets/img/background/signIn.png')}>
        <StatusBar hidden={true} />
        <TouchableOpacity style={styles.skip} onPress={() => navigate('Home')}>
            <TextRegular text='Skip' color={THEME.WHITE_COLOR} />
        </TouchableOpacity>
        <View style={styles.container}>
            <Text style={{ ...styles.text, color: THEME.WHITE_COLOR }}>
                {`Hi ${userInfo.login},\nWelcom to\n`}
                <Text style={{ ...styles.text, color: '#FFCC00' }}>
                    FoodPost
                </Text>
            </Text>
            <View style={styles.rule}>
                <TextRegular
                    color={THEME.WHITE_COLOR}
                    text='Please turn on you GPS to find out better restauran suggestions near you.' />
            </View>
            <Button text='Turn On GPS' onPress={getPosition} />
        </View>
    </ImageHOC>
}

const styles = StyleSheet.create({
    container: {
        width: '100%',
        justifyContent: 'center',
        alignItems: 'flex-start',
        paddingHorizontal: 30
    },
    skip: {
        paddingVertical: 5,
        paddingHorizontal: 20,
        borderRadius: 10,
        position: 'absolute',
        top: 30,
        right: 25,
        backgroundColor: 'rgba(255, 255, 255, .5)'
    },
    text: {
        fontSize: 30,
        fontFamily: THEME.JOSEFINSANS_REGULAR,
    },
    rule: {
        marginVertical: 50
    }
})