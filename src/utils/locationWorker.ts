import { getDistance } from 'geolib'

interface Coords {
    latitude: number,
    longitude: number
}

export const getDistanceKM = (start: Coords, end: Coords) =>
    Math.round(getDistance(start, end) / 1000)