import AsyncStorage from "@react-native-community/async-storage"

export const setAsyncStorageTokens = async (token: string, refreshToken: string) => {
    await AsyncStorage.setItem('@fp_token', token)
    await AsyncStorage.setItem('@fp_refToken', refreshToken)
}

export const getAsyncStorageTokens = async (): Promise<IResponseTokensStorage> => ({
    token: await AsyncStorage.getItem('@fp_token') || '',
    refreshToken: await AsyncStorage.getItem('@fp_refToken') || ''
})

export const deleteAllAsyncStorageValue = async() => {
    await AsyncStorage.removeItem('@fp_token')
    await AsyncStorage.removeItem('@fp_refToken')
    await AsyncStorage.removeItem('@fp_email')
}

export interface IResponseTokensStorage {
    token: string
    refreshToken: string
}