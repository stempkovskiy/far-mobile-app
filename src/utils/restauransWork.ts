import { Photo, Restauran, TimeWork } from '../api/restauran'

export const filter = (restaurans: Restauran[], categories: string[], rating: number, distance: number) => {
    let array = filterCategory(restaurans, categories)
    array = filterDistance(array, distance)
    array = filterRating(array, rating)
    return array
}

export const filterCategory = (restaurans: Restauran[], category: string[]) =>
    restaurans.filter(restauran => category.includes(restauran.kitchen))

export const filterRating = (restaurans: Restauran[], rating: number) =>
    restaurans.filter(restauran => restauran.rating === rating)

export const filterDistance = (restauran: Restauran[], distance: number) =>
    restauran.filter(restauran => restauran.km <= distance)

export const getRoundedDivNumber = (value: number, div: number) =>
    Math.round(value / div)

export const getRandomInt = (min: number, max: number) => {
    min = Math.ceil(min)
    max = Math.floor(max)
    return Math.floor(Math.random() * (max - min)) + min
}

const days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday']

export enum daliTimeEnum {
    wokr,
    notWork,
    unknown
}

export interface IDalyTimeResponse {
    state: daliTimeEnum,
    currentTime?: TimeWork
}

export const dailyTime = (timeWorks: TimeWork[]): IDalyTimeResponse => {
    const date = new Date()
    const currentDay = days[date.getDay()]
    const currentTimeWork = timeWorks.find(time => currentDay === time.day)
    const currentHour = date.getHours()
    if (currentTimeWork) {
        const beginHourWorkTime = +currentTimeWork.beginWork.slice(0, 2)
        const endHourWorkTime = +currentTimeWork.endWork.slice(0, 2)
        if (currentHour > beginHourWorkTime && currentHour < endHourWorkTime) {
            return {
                currentTime: currentTimeWork,
                state: daliTimeEnum.wokr
            }
        }
        return {
            currentTime: currentTimeWork,
            state: daliTimeEnum.notWork
        }
    }
    return {
        state: daliTimeEnum.unknown
    }
}