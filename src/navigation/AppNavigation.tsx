import React, { useContext, useLayoutEffect } from "react"
import { NavigationContainer, useNavigation, useRoute } from "@react-navigation/native"
import { createStackNavigator, CardStyleInterpolators, StackHeaderProps } from "@react-navigation/stack"
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
// Screens
import { Home } from "../screens/home/Home"
import { SignIn } from "../screens/SignIn"
import { Registarion } from "../screens/Registration"
import { AuthContext } from '../context/auth/authContext'
import { Profile } from "../screens/profile/Profile"
import { Favourite } from "../screens/favourite/Favourite"
import { Review } from "../screens/review/Review"
import { Notification } from "../screens/notification/Notification"
import { TrendingRestaurants } from "../screens/home/TrendingRestaurant"
import { DetalisRestauran } from "../screens/home/detail/DetailsRestaurnt"
import { MyTabBar } from "./MyTabBar"
import { ReviweAndRating } from "../screens/home/ReviewAndRatings"
import { Filter } from "../screens/home/filter/Filter"
import { MyHeader } from "../components/UI/headers/MyHeader"
import { Categories } from "../screens/home/category/Categories"
import { Category } from "../screens/home/category/Category"
import { CustomHeader } from "../components/UI/headers/CustomHeader"
import { ListPageInfo } from "../components/ListPageInfo"
import { TextRegular } from "../components/UI/TextRegular"
import { MenuPhotos } from "../screens/home/detail/MenuPhotos"
import { PremissionGPS } from "../screens/PremissionGPS"
import { View } from "react-native"

const AuthStack = createStackNavigator()
const MainNavigation = createStackNavigator()
const TabNavigation = createBottomTabNavigator()



export const AppNavigation: React.FC = () => {
  const { token, } = useContext(AuthContext)

  const TabStackNavigation = () => {
    return <TabNavigation.Navigator
      tabBar={props => <MyTabBar {...props} />}
      tabBarOptions={
        {
          keyboardHidesTabBar: true,
          showLabel: false,
          adaptive: false,
        }}>
      <TabNavigation.Screen
        name='Home'
        component={Home} />
      <TabNavigation.Screen
        name='Favourite'
        component={Favourite} />
      <TabNavigation.Screen
        name='Review'
        component={Review} />
      <TabNavigation.Screen
        name='Notification'
        component={Notification} />
      <TabNavigation.Screen
        name='Profile'
        component={Profile} />

    </TabNavigation.Navigator>
  }

  return (
    <NavigationContainer>
      {
        token !== ''
          ?
          <MainNavigation.Navigator
            screenOptions={{ cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS }}
          >
            <MainNavigation.Screen
              name='Home'
              options={{
                header: (props: StackHeaderProps) =>
                  <CustomHeader props={props} />,
              }}
              component={TabStackNavigation}
            />
            <MainNavigation.Screen
              name='Trending'
              component={TrendingRestaurants}
              options={{ header: () => <MyHeader title='Trending Restaurants' showBtnClose={false} /> }} />
            <MainNavigation.Screen
              name='Details'
              component={DetalisRestauran}
              options={{ headerShown: false }} />
            <MainNavigation.Screen
              name='ReviewAndRatings'
              component={ReviweAndRating}
              options={{ header: () => <MyHeader title='Review & Ratings' /> }} />
            <MainNavigation.Screen
              name='Filter'
              component={Filter}
              options={{ header: () => <MyHeader title='Filter' /> }} />
            <MainNavigation.Screen
              name='Categories'
              component={Categories}
              options={{ header: () => <MyHeader title='Category' showBtnClose={false} /> }} />
            <MainNavigation.Screen
              name='Category'
              component={Category}
              options={{ headerShown: false }} />
            <MainNavigation.Screen
              name='Info'
              component={ListPageInfo}
              options={{
                header: (props: StackHeaderProps) =>
                  <MyHeader title='' showBtnClose={false} props={props} />
              }} />
            <MainNavigation.Screen
              name='Menu&Photos'
              component={MenuPhotos}
              options={{ header: () => <MyHeader title='Menu&Photos' showBtnClose={false} /> }}
            />
            <MainNavigation.Screen
              name='PermissionGPS'
              component={PremissionGPS}
              options={{header: () => <></>}}
            />
          </MainNavigation.Navigator>
          : <AuthStack.Navigator
            headerMode='none'
            screenOptions={{ cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS }}>
            <AuthStack.Screen name='SignIn' component={SignIn} />
            <AuthStack.Screen name='Registration' component={Registarion} />
          </AuthStack.Navigator>
      }
    </NavigationContainer>
  )
}
