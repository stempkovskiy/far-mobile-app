import React, { useState, useEffect, useContext } from 'react'
import {
    StyleSheet,
    View,
    Dimensions,
    TouchableOpacity,
    Image,
    Keyboard,
    Animated
} from 'react-native'
import { AntDesign } from '@expo/vector-icons'
import { THEME } from '../theme'
import { BottomTabBarProps } from '@react-navigation/bottom-tabs'
import { AuthContext } from '../context/auth/authContext'

export const MyTabBar: React.FC<BottomTabBarProps> = ({ state, navigation }) => {
    const [isKeyboardVisible, setKeyboardVisible] = React.useState(false)
    const animateRotation = useState(new Animated.Value(0))[0]

    useEffect(() => {
        if (state.index !== 1) {
            const keyboardDidShowListener = Keyboard.addListener(
                'keyboardDidShow',
                () => {
                    setKeyboardVisible(true)
                },
            )
            const keyboardDidHideListener = Keyboard.addListener(
                'keyboardDidHide',
                () => {
                    setKeyboardVisible(false)
                },
            )

            return () => {
                keyboardDidHideListener.remove()
                keyboardDidShowListener.remove()
            }
        }
    }, [state.index])

    return (
        !isKeyboardVisible ? <View style={{ ...styles.tabContainer }}>
            <View style={styles.tabBar}>
                <TouchableOpacity
                    style={styles.tab}
                    activeOpacity={.5}
                    onPress={() => {
                        navigation.navigate('Home')
                        Animated.timing(animateRotation, {
                            toValue: 0,
                            duration: 300,
                            useNativeDriver: false,
                        }).start()
                    }} >
                    {
                        state.index !== 0
                            ? <Image source={THEME.HOME_IMG} resizeMode='center' />
                            : <Image source={THEME.HOME_ACTIVE_IMG} resizeMode='center' />
                    }
                </TouchableOpacity>
                <TouchableOpacity
                    style={styles.tab}
                    activeOpacity={.5}
                    onPress={() => {
                        navigation.navigate('Favourite')
                        Animated.timing(animateRotation, {
                            toValue: 0,
                            duration: 300,
                            useNativeDriver: false,
                        }).start()
                    }} >
                    {
                        state.index !== 1
                            ? <Image source={THEME.FAVOURITE_IMG} resizeMode='center' />
                            : <Image source={THEME.FAVOURITE_ACTIVE_IMG} resizeMode='center' />
                    }
                </TouchableOpacity>
                <View style={{
                    flex: 1,
                    alignSelf: 'stretch',
                    width: Dimensions.get('window').width / 5,
                }} />
                <TouchableOpacity
                    style={styles.tab}
                    activeOpacity={.5}
                    onPress={() => {
                        navigation.navigate('Notification')
                        Animated.timing(animateRotation, {
                            toValue: 0,
                            duration: 300,
                            useNativeDriver: false,
                        }).start()
                    }}>
                    {
                        state.index !== 3
                            ? <Image source={THEME.NOTIFICATION_IMG} resizeMode='center' />
                            : <Image source={THEME.NOTIFICATION_ACTIVE_IMG} resizeMode='center' />
                    }
                </TouchableOpacity>
                <TouchableOpacity
                    style={styles.tab}
                    activeOpacity={.5}
                    onPress={() => {
                        navigation.navigate('Profile')
                        Animated.timing(animateRotation, {
                            toValue: 0,
                            duration: 300,
                            useNativeDriver: false,
                        }).start()
                    }}>
                    {
                        state.index !== 4
                            ? <Image source={THEME.PROFILE_IMG} resizeMode='center' />
                            : <Image source={THEME.PROFILE_ACTIVE_IMG} resizeMode='center' />
                    }
                </TouchableOpacity>
            </View>
            <TouchableOpacity
                style={styles.plusBtn}
                activeOpacity={.5}
                onPress={() => {
                    navigation.navigate('Review')
                    Animated.timing(animateRotation, {
                        toValue: 45,
                        duration: 300,
                        useNativeDriver: false,
                    }).start()
                }}>
                <Animated.View style={{ rotation: animateRotation }}>
                    <AntDesign name='plus' color={THEME.WHITE_COLOR} size={30} />
                </Animated.View>
            </TouchableOpacity>
        </View>
            : <></>
    )
}

const styles = StyleSheet.create({
    tabContainer: {
        height: 75,
        justifyContent: 'flex-end',
        alignItems: 'center',
        position: 'absolute',
        bottom: 0,
        right: 0,
        left: 0,
    },
    tabBar: {
        elevation: 10,
        height: 50,
        alignSelf: 'stretch',
        flexDirection: 'row',
        backgroundColor: THEME.WHITE_COLOR,
        justifyContent: 'space-evenly',
        alignItems: 'center',
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
    },
    tab: {
        flex: 1,
        alignSelf: 'stretch',
        width: Dimensions.get('window').width / 5,
        justifyContent: 'center',
        alignItems: 'center',
    },
    plusBtn: {
        elevation: 15,
        position: 'absolute',
        top: 0,
        height: 60,
        width: 60,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#5663FF',
        borderRadius: 30,
    }
})