import { RouteProp } from '@react-navigation/native';
import { StackNavigationProp } from '@react-navigation/stack';

export type RootStackParamList = {
    Home: undefined
    SignIn: undefined
}

export type SignInParamList = {
    SignIn: undefined,
    Registration: undefined,
    ForgotPassword: undefined
}

export type SignInProps = {
  route: RouteProp<SignInParamList, 'Registration'>
  navigation: StackNavigationProp<SignInParamList, 'Registration'>;
}