import { BaseRouter } from "@react-navigation/native";
import { getAsyncStorageTokens, setAsyncSorageTokens } from "../utils/asyncStorageWorker";
import { AuthSuccessResponse } from "./authorization";
import { BASE_URL } from "./config";

export enum Errors {
    InternalServerError = 500,
    ServiceUnavailable = 503,
    BadRequest = 400,
    Unauthorized = 401,
    NotFound = 404,
}


export const warapFailer = async (error: Errors): Promise<FailerResponse> => {
    let response: FailerResponse = {
        message: '',
        success: false
    }
    switch (error) {
        case Errors.Unauthorized:
            response = await refreshTokenAsync()
            break;
        case Errors.BadRequest:
            response = { message: 'Not correct send request', success: false }
            break;
        case Errors.NotFound:
            response = { message: 'No such page exists', success: false }
            break;
        case Errors.InternalServerError:
            response = { message: 'Server error. Try late again.', success: false }
            break;
        case Errors.ServiceUnavailable:
            response = { message: 'Service unavailable.', success: false }
            break;

    }
    return response;
}

export const refreshTokenAsync = async (): Promise<FailerResponse> => {
    try {
        const { refreshToken, token } = await getAsyncStorageTokens()
        const body = {
            Token: token,
            RefreshToken: refreshToken
        }
        const response = await fetch(`${BASE_URL}api/v1/identity/refresh`, {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(body)
        })
        if (response.ok) {
            const data = await response.json() as AuthSuccessResponse
            await setAsyncSorageTokens(data.token, data.refreshToken)
            return {
                success: true
            }
        }
        const message = await response.json()
        return {
            success: false,
            message: message['errors'][0]
        }
    } catch (error) {
        throw error
    }
}
export interface FailerResponse {
    success: boolean,
    message?: string
}

