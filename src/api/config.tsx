import AsyncStorage from "@react-native-community/async-storage"

export const BASE_URL = 'https://far.azurewebsites.net/'
export const getHeaders = async () => {
    return {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${await AsyncStorage.getItem('@fp_token')}`,
    }
}


export interface BaseTypeResponse<T> {
    success: boolean,
    message?: string[],
    data?: T
}