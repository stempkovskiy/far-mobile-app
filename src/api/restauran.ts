import { BaseTypeResponse, BASE_URL, getHeaders } from "./config"
import { warapFailer } from "./failerResponser"

export async function getAllRestauranAsync(): Promise<BaseTypeResponse<ResponseAllRestaurans>> {
    try {
        const response = await fetch(`${BASE_URL}api/v1/restaurants/getAllRestaurants`, {
            method: 'GET',
            headers: await getHeaders()
        })
        if (response.ok) {
            return {
                data: await response.json() as ResponseAllRestaurans,
                success: true
            }
        }
        const isUpdateToken = await warapFailer(response.status)
        if (isUpdateToken.success) {
          await getAllRestauranAsync()   
        }
        return {
            message: [isUpdateToken.message!],
            success: isUpdateToken.success
        }
    } catch (error) {
        throw error
    }
}

export async function getRestauranByIdAsync(id: string): Promise<ResponseRestauran> {
    try {
        const response = await fetch(`${BASE_URL}api/v1/restaurants/findRestauran?id=${id}`, {
            method: 'GET',
            headers: await getHeaders()
        })
        return await response.json() as ResponseRestauran
    } catch (error) {
        throw error
    }
}


export type ResponseAllRestaurans = {
    count?: number,
    restaurans?: Restauran[]
}

export type ResponseRestauran = {
    success: boolean,
    message: string[]
    restauran: Restauran[]
}

export type Restauran = {
    id: string,
    name: string,
    phone: string,
    kitchen: string
    km: number,
    rating: number
    street: string,
    photos: Photo[],
    menuPhoto: Photo[],
    timeWorks: TimeWork[],
    isFavouvarit: boolean,
    reviews: Reviews[]
}
export type Reviews = {
    id: string,
    name: string,
    rating: number,
    restauranId: string,
    restauranPhoto: string,
    avatar: string,
    resturan: string,
    text: string
}


export type Photo = {
    id: string,
    path: string
    name: string,
}

export type TimeWork = {
    beginWork: string,
    endWork: string,
    day: string,
    id: string
}