import { FollowingType, UserInfo } from "../context/app/appContenxt"
import { AuthSuccessResponse } from "./authorization"
import { BaseTypeResponse, BASE_URL, getHeaders } from "./config"
import { warapFailer } from "./failerResponser"

export async function addReviewAsync(body: NewReviewRequest): Promise<BaseTypeResponse<AuthSuccessResponse>> {
    try {
        const response = await fetch(`${BASE_URL}api/v1/user/addReview`, {
            method: 'POST',
            headers: await getHeaders(),
            body: JSON.stringify(body)
        })
        if (response.ok) {
            return {
                success: true,
                data: await response.json() as AuthSuccessResponse
            }
        }
        const isUpdateToken = await warapFailer(response.status)
        if (isUpdateToken.success) {
            await addReviewAsync(body)
        }
        return {
            message: [isUpdateToken.message!],
            success: isUpdateToken.success
        }

    } catch (error) {
        throw error
    }
}

export async function getAllUsersAsync(): Promise<UserInfo> {
    try {
        const response = await fetch(`${BASE_URL}api/v1/user/getAllUsers`, {
            method: 'GET',
            headers: await getHeaders()
        })
        return await response.json() as UserInfo
    } catch (error) {
        throw error
    }
}

export async function getAllUnFollowUsersAsync(idUser: string): Promise<AllUnFollowUsersResponse> {
    try {
        const response = await fetch(`${BASE_URL}api/v1/user/getAllNonFollowUsers?Id=${idUser}`, {
            method: 'GET',
            headers: await getHeaders(),
        })
        return await response.json() as AllUnFollowUsersResponse
    } catch (error) {
        throw error
    }
}

export async function followAsync(body: FollowRequest): Promise<BaseTypeResponse<void>> {
    try {
        const response = await fetch(`${BASE_URL}api/v1/user/follow`, {
            method: 'POST',
            headers: await getHeaders(),
            body: JSON.stringify(body)
        })
        return await response.json() as BaseTypeResponse<void>
    } catch (error) {
        throw error
    }
}

export async function unFollowAsync(body: FollowRequest): Promise<BaseTypeResponse<void>> {
    try {
        const response = await fetch(`${BASE_URL}api/v1/user/unfollow`, {
            method: 'DELETE',
            headers: await getHeaders(),
            body: JSON.stringify(body)
        })
        return await response.json() as BaseTypeResponse<void>
    } catch (error) {
        throw error
    }
}

export async function getUserAsync(value: string, type: GetUserEnum): Promise<BaseTypeResponse<UsersResponse>> {
    try {
        const params = type == GetUserEnum.email ? `email=${value}` : `Id=${value}`
        const response = await fetch(`${BASE_URL}api/v1/user/${type}?${params}`, {
            method: 'GET',
            headers: await getHeaders()
        })
        if (response.ok) {
            return {
                success: true,
                data: await response.json() as UsersResponse
            }
        }
        const isUpdateToken = await warapFailer(response.status)
        if (isUpdateToken.success) {
            getUserAsync(value, type)
        }
        return {
            success: isUpdateToken.success,
            message: [isUpdateToken.message![0]]
        }
    } catch (error) {
        throw error
    }
}

export enum GetUserEnum {
    email = 'getUserByEmail',
    id = 'getUser'
}

export type FollowRequest = {
    IdFollow: string,
    IdUser: string,
}

export type AllUnFollowUsersResponse = {
    success: boolean,
    followings: FollowingType[]
}
export type UsersResponse = {
    success: boolean,
    userInfos: UserInfo[]
}

export type NewReviewRequest = {
    IdUser: string,
    IdRestauran: string,
    Text: string,
    Rating: number
}