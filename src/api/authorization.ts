import { UserInfo } from '../context/app/appContenxt'
import { ImageInfo } from '../screens/Registration'
import { BASE_URL } from './config'

export async function signInAsync(body: RequestSignIn): Promise<AuthSuccessResponse> {
    try {
        const response = await fetch(BASE_URL + 'api/v1/identity/login', {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(body)
        })
        return await response.json() as AuthSuccessResponse
    } catch (error) {
        throw new Error(error)
    }
}

export async function registrationAsync(body: IUserRegistrationRequest): Promise<any> {
    try {
        const formData = new FormData()
        for (const key in body) {
            if (Object.prototype.hasOwnProperty.call(body, key)) {
                formData.append(key, body[key])
            }
        }
        const response = await fetch(BASE_URL + 'api/v1/identity/register', {
            method: 'POST',
            body: formData
        })
        return await response.json()
    } catch (error) {
        throw new Error(error)
    }
}

export interface IUserRegistrationRequest {
    [key: string]: string | ImageInfo,
    Avatar: ImageInfo,
    Name: string,
    Email: string,
    Password: string,
}

export type RequestSignIn = {
    Email: string,
    Password: string
}

export type AuthSuccessResponse = {
    refreshToken: string,
    token: string,
    userInfo: UserInfo,
    errors: string[]
}
