export const errorMessage = {
    INVALID_EMAIL: 'Invalid email address',
    INVALID_PASSWORD: 'Invalid password entered',
    INVALID_LOGIN_LENGTH: 'Login length must not be less than three letters',
    INVALID_CONFIRM_PASSWORD: 'Password does not match'
}