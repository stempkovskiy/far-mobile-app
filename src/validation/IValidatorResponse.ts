export interface IValidatorResponse {
    success: boolean,
    errorMessage : string[]
}

export class ValidationResponse implements IValidatorResponse {
    success = false
    errorMessage: string[] = []
}