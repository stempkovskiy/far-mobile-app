import { ValidationResponse } from "./IValidatorResponse"
import { errorMessage } from "./errorMessage"
import { registerRootComponent } from "expo"

export const passwordValidator = (password: string): ValidationResponse | boolean => {
    const regexp = new RegExp(/^.*(?=.{8,})(?=.*[a-zA-Z])(?=.*\d)(?=.*[!#$%&? "]).*$/)
    if (!regexp.test(password)) {
        const errorValid = new ValidationResponse()
        errorValid.success = false,
            errorValid.errorMessage = [errorMessage.INVALID_PASSWORD]
        return errorValid
    } else {
        return true
    }
}

export const emailValidator = (email: string): ValidationResponse | boolean => {
    const regexp = new RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)
    if (!regexp.test(email)) {
        const errorValid = new ValidationResponse()
        errorValid.success = false,
            errorValid.errorMessage = [errorMessage.INVALID_EMAIL]
        return errorValid
    } else {
        return true
    }
}

export const loginValidator = (login: string): ValidationResponse | boolean => {
    if (login.trim() && login.length <= 3) {
        const error = new ValidationResponse()
        error.errorMessage = [errorMessage.INVALID_LOGIN_LENGTH],
            error.success = false
        return error
    }
    return true
}

export const isEmptyFields = (str: string[]): boolean => {
    let count = 0
    str.forEach(current => {
        if (!current.trim())
            return count++
    })
    return count > 0 ? false : true
}

export const confirmPasswordValidator =
    (password: string, currentPassword: string): ValidationResponse | boolean => {
        if (password !== currentPassword) {
            const error = new ValidationResponse()
            error.errorMessage = [errorMessage.INVALID_CONFIRM_PASSWORD],
                error.success = false
            return error
        }
        return true
    }