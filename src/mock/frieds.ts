
export type Friends = {
    id: string,
    img: any
}

export const friendsMockData: Friends[] = [
    {
        id: '0',
        img: require('../../assets/img/avatars/1.png'),
    },
    {
        id: '1',
        img: require('../../assets/img/avatars/2.png'),
    },
    {
        id: '2',
        img: require('../../assets/img/avatars/3.png'),
    },
    {
        id: '3',
        img: require('../../assets/img/avatars/4.png'),
    },
    {
        id: '4',
        img: require('../../assets/img/avatars/5.png'),
    },
    {
        id: '5',
        img: require('../../assets/img/avatars/6.png'),
    },
    {
        id: '6',
        img: require('../../assets/img/avatars/7.png'),
    },
    {
        id: '7',
        img: require('../../assets/img/avatars/8.png'),
    },
]