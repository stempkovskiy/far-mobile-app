export type Category = {
    id: string,
    name: string,
    img: any,
    linerGradient: string[]
}

export const categoryMokData: Category[] = [
    {
        id: '0',
        name: 'Italian',
        img: require('../../assets/img/restaurants/1.jpg'),
        linerGradient: ['#FF5673', '#FF8C48']
    },
    {
        id: '1',
        name: 'Chinese',
        img: require('../../assets/img/restaurants/2.jpg'),
        linerGradient: ['#832BF6', '#FF4665']
    },
    {
        id: '2',
        name: 'Mexican',
        img: require('../../assets/img/restaurants/3.jpg'),
        linerGradient: ['#2DCEF8', '#3B40FE']
    },
    {
        id: '3',
        name: 'Thai',
        img: require('../../assets/img/restaurants/4.jpg'),
        linerGradient: ['#009DC5', '#21E590']
    },
    {
        id: '4',
        name: 'Arabian',
        img: require('../../assets/img/restaurants/5.jpg'),
        linerGradient: ['#FF870E', '#D236D2']
    },
    {
        id: '5',
        name: 'India',
        img: require('../../assets/img/restaurants/6.jpg'),
        linerGradient: ['#FE327E', '#5C51FF']
    },
    {
        id: '6',
        name: 'American',
        img: require('../../assets/img/restaurants/7.jpg'),
        linerGradient: ['#2CE3F1', '#6143FF']
    },
    {
        id: '7',
        name: 'Korea',
        img: require('../../assets/img/restaurants/1.jpg'),
        linerGradient: ['#FF5673', '#FF8C48']
    },
    {
        id: '8',
        name: 'European',
        img: require('../../assets/img/restaurants/1.jpg'),
        linerGradient: ['#832BF6', '#FF4665']
    }
]
