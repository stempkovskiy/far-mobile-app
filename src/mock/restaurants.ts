import { IRestaurans } from "../components/CardRestaurans"

//Italian = 1
//European = 2
//Ispania = 1
//Chinese = 1
//Indian = 1
//Thai = 1
//Mexican = 1




export const restauransMockData: IRestaurans[] = [
    {
        id: 0,
        state: 'OPEN',
        rating: 4.5,
        img: require('../../assets/img/restaurants/1.jpg'),
        name: 'Happy Bonnes',
        kitchen: 'Italian',
        distance: 50,
        street: '394 Broome ST, New York, NY 10013, USA',
        isFavouvarit: false
    },
    {
        id: 1,
        state: 'CLOSE',
        rating: 3.5,
        img: require('../../assets/img/restaurants/2.jpg'),
        name: 'Le Bernardin',
        kitchen: 'European',
        distance: 20,
        street: '155 W 51st St',
        isFavouvarit: false
    },
    {
        id: 2,
        state: 'OPEN',
        rating: 3.5,
        img: require('../../assets/img/restaurants/3.jpg'),
        name: 'The Modern',
        kitchen: 'Ispania',
        distance: 18.1,
        street: '9 W 53rd St',
        isFavouvarit: false
    },
    {
        id: 3,
        state: 'OPEN',
        rating: 2.5,
        img: require('../../assets/img/restaurants/12.jpg'),
        name: 'Zuma',
        kitchen: 'Chinese',
        distance: 10,
        street: '261 Madison Ave, New York, NY 10016',
        isFavouvarit: false
    },
    {
        id: 4,
        state: 'CLOSE',
        rating: 5,
        img: require('../../assets/img/restaurants/11.jpg'),
        name: 'Memphis Seoul',
        kitchen: 'India',
        distance: 35,
        street: '569 Lincoln Pl, Brooklyn, NY 11238',
        isFavouvarit: false
    },
    {
        id: 5,
        state: 'CLOSE',
        rating: 4,
        img: require('../../assets/img/restaurants/10.jpg'),
        name: 'Le Bernardin',
        kitchen: 'Thai',
        distance: 11.7,
        street: '155 W 51st St, New York, NY 10019',
        isFavouvarit: false
    },
    {
        id: 6,
        state: 'OPEN',
        rating: 3.5,
        img: require('../../assets/img/restaurants/9.jpg'),
        name: 'Daniel',
        kitchen: 'Mexican',
        distance: 7.9,
        street: '60 E 65th St, New York, NY 10065',
        isFavouvarit: false
    },
    {
        id: 7,
        state: 'OPEN',
        rating: 5,
        img: require('../../assets/img/restaurants/8.jpg'),
        name: 'Wildair.',
        kitchen: 'European',
        distance: 33,
        street: '142 Orchard St, New York, NY 10002',
        isFavouvarit: false
    },
]