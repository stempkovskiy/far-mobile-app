import { ViewStyle, StyleProp } from "react-native";

export interface ITextLabel {
    text: string,
    color?: string,
    fontSize?: number,
    style?: ViewStyle
}
