import React, { useContext, useEffect } from 'react'
import { useNavigation, useRoute } from '@react-navigation/native'
import { View } from 'react-native'
import { FlatList } from 'react-native-gesture-handler'
import { ReviewUser } from '../screens/home/detail/ReviewUser'
import { THEME } from '../theme'
import { User, UsersListType } from './User'
import { AppContext, FollowingType, UserInfo, } from '../context/app/appContenxt'
import { Reviews } from '../api/restauran'

export enum ListPageHeaderEnum {
    Reviews = 'Reviews',
    Followers = 'Followers',
    Followings = 'Followings',
    Users = 'Users'
}

export type ListPageInfoType = {
    header: string,
    users?: FollowingType[],
    reviews?: Reviews[]
}

export const ListPageInfo: React.FC = () => {
    const { userInfo } = useContext(AppContext)
    const { setOptions } = useNavigation()
    const { header, users, reviews } = useRoute().params as ListPageInfoType
    useEffect(() => { setOptions({ title: header }) }, [])

    const createFllatList = () => {
        const idFollowers = userInfo.followers.map(follower => follower.idFollow)
        const idFollowings = userInfo.following.map(following => following.idFollowing)
        switch (header) {
            case ListPageHeaderEnum.Reviews:
                return <FlatList
                    style={{ flex: 1, backgroundColor: THEME.WHITE_COLOR }}
                    data={!reviews?.length ? userInfo.reviews : reviews}
                    keyExtractor={({ id }) => id}
                    renderItem={({ item }) =>
                        <ReviewUser
                            name={!reviews?.length ? item.resturan : item.name}
                            avatar={!reviews?.length ? item.restauranPhoto : item.avatar}
                            rating={item.rating}
                            review={item.text}
                        />
                    }
                />
            case ListPageHeaderEnum.Followers:
                const userList = idFollowers.map(id => {
                    const user = userInfo.followers.find(f => f.idFollow === id)
                    const info: UsersListType = {
                        idFollow: user?.idFollow!,
                        avatar: user?.avatar!,
                        countReview: +user?.countReview!,
                        login: user?.login!,
                        subscribe: idFollowings.includes(id)
                    }
                    return info
                })
                return <FlatList
                    style={{ flex: 1, backgroundColor: THEME.WHITE_COLOR }}
                    data={userList as UsersListType[]}
                    keyExtractor={({ idFollow }) => idFollow}
                    renderItem={({ item }) =>
                        <User
                            idFollow={item.idFollow}
                            avatar={item.avatar}
                            countReview={item.countReview}
                            login={item.login}
                            subscribe={item.subscribe}
                        />
                    }
                />
            case ListPageHeaderEnum.Followings:
                return <FlatList
                    style={{ flex: 1, backgroundColor: THEME.WHITE_COLOR }}
                    data={userInfo.following}
                    keyExtractor={({ id }) => id}
                    renderItem={({ item }) =>
                        <User
                            idFollow={item.idFollowing}
                            avatar={item.avatar}
                            countReview={+item.countReview}
                            login={item.login}
                            subscribe={true}
                        />
                    }
                />
            case ListPageHeaderEnum.Users:
                return <FlatList
                    style={{ flex: 1, backgroundColor: THEME.WHITE_COLOR }}
                    data={users}
                    keyExtractor={({ id }) => id}
                    renderItem={({ item }) =>
                        <User
                            subscribe={idFollowings.includes(item.idFollowing)}
                            idFollow={item.idFollowing}
                            avatar={item.avatar}
                            countReview={+item.countReview}
                            login={item.login}
                        />
                    }
                />
        }
    }

    return <View style={{ flex: 1 }}>
        {createFllatList()}
    </View>
}