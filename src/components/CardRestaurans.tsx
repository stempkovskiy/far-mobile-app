import React, { useContext, useState } from 'react'
import { View, StyleSheet, Dimensions, ImageBackground, ViewStyle, TouchableOpacity, Image } from 'react-native'
import { TextRegular } from './UI/TextRegular'
import { TextBold } from './UI/TextBold'
import { LinearGradient } from 'expo-linear-gradient'
import { AntDesign, FontAwesome } from '@expo/vector-icons'
import { THEME } from '../theme'
import { useNavigation } from '@react-navigation/native'
import { getRestauranByIdAsync, Restauran } from '../api/restauran'
import { AppContext } from '../context/app/appContenxt'
import { ToastTypeEnum } from './UI/Toast'
import { dailyTime, daliTimeEnum } from '../utils/restauransWork'

export type RestauranInfo = {
    info: Restauran,
    style?: ViewStyle,
    isBtnPlus?: boolean,
    onBtnPlus?: (id: string) => void
}

export const CardRestaurans: React.FC<RestauranInfo> = ({ info, isBtnPlus = false, style, onBtnPlus }) => {
    const navigation = useNavigation()
    const [select, setSelect] = useState(false)
    const { initToast } = useContext(AppContext)
    let btnPlus
    if (isBtnPlus) {
        btnPlus = <TouchableOpacity
            style={{
                ...styles.btnPlus,
                backgroundColor: select ? '#4CD964' : THEME.BTN_BACKGROUND_COLOR
            }}
            onPress={() => {
                setSelect(!select)
                onBtnPlus!(info.id)
            }}
        >
            {
                !select
                    ? <AntDesign name='plus' color={THEME.WHITE_COLOR} size={20} />
                    : <AntDesign name='check' size={20} color={THEME.WHITE_COLOR} />
            }
        </TouchableOpacity>
    }

    const getRestauranById = async () => {
        const response = await getRestauranByIdAsync(info.id)
        if (response.success) {
            navigation.navigate('Details', response.restauran)
        } else {
            initToast({ hide: false, text: response.message[0], type: ToastTypeEnum.error })
        }
    }
    const getState = () => {
        const response = dailyTime(info.timeWorks)
        if (response.state === daliTimeEnum.wokr) {
            return <TextBold
                text='OPEN'
                color={THEME.TEXT_STATE_OPEN} />
        } else if (response.state === daliTimeEnum.notWork) {
            return <TextBold
                text='CLOSE'
                color={THEME.TEXT_STATE_CLOSE} />
        } else {
            return <TextBold
                text='UNKNOWN'
                color={THEME.TEXT_STATE_UNKNOWN} />
        }
    }
    return (
        <TouchableOpacity
            activeOpacity={.8}
            onPress={getRestauranById}
            style={{ ...styles.container, ...style }}>
            <ImageBackground
                style={styles.backgroundImg}
                source={{ uri: info.photos[0].path }}
                borderTopLeftRadius={10}
                borderTopRightRadius={10}>
                <View style={styles.mode}>
                    {
                        getState()
                    }
                </View>
                <View style={styles.rating}>
                    <FontAwesome
                        name='star'
                        color='orange' size={20} />
                    <TextBold
                        text={`${info.rating}`}
                        fontSize={18} />
                </View>
            </ImageBackground>
            <View style={styles.footer}>
                <View style={styles.Info}>
                    <TextBold
                        text={info.name}
                        fontSize={18} />
                    <View style={styles.contentInfoRounded}>
                        <LinearGradient
                            style={{ borderRadius: 8, paddingHorizontal: 5 }}
                            colors={THEME.GRADIENTS.KITCHEN_INFO}
                        >
                            <TextRegular text={info.kitchen} color={THEME.WHITE_COLOR} fontSize={11} />
                        </LinearGradient>
                    </View>
                    <View style={{ ...styles.contentInfoRounded, backgroundColor: THEME.GRADIENTS.DISTANCE_INFO }} >
                        <View style={{ borderRadius: 8, paddingHorizontal: 5 }}>
                            <TextRegular text={`${info.km ?? 0} km`} color={THEME.WHITE_COLOR} fontSize={11} />
                        </View>
                    </View>
                    <View style={styles.lastFourReviewAvatar}>
                        {
                            info.reviews
                                .slice(0, 4)
                                .map((item, index) =>
                                    <Image
                                        key={item.id}
                                        style={{
                                            ...styles.avatrReview,
                                            left: index * 13,
                                        }}
                                        source={{ uri: item.avatar }} />)
                        }
                    </View>
                </View>
                <TextRegular text={info.street} color={THEME.TEXT_COLOR} />
                {
                    btnPlus
                }
            </View>
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    container: {
        marginTop: 20,
        marginLeft: 10,
        marginRight: 10,
        display: 'flex',
        borderRadius: 10,
        backgroundColor: '#fff',
        height: Dimensions.get('window').height / 3,
        width: Dimensions.get('window').width / 1.2,
        elevation: 3,
    },
    backgroundImg: {
        flex: 3,
        padding: 10,
        justifyContent: 'space-between',
        flexDirection: 'row'
    },
    mode: {
        height: 30,
        padding: 5,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#fff',
        borderRadius: 5,
        elevation: 3,
    },
    rating: {
        justifyContent: 'space-evenly',
        alignItems: 'center',
        flexDirection: 'row',
        width: 60,
        height: 30,
        backgroundColor: '#fff',
        borderRadius: 5,
        elevation: 3,
    },
    footer: {
        flex: 1,
        backgroundColor: '#fff',
        borderBottomLeftRadius: 10,
        borderBottomRightRadius: 10,
        padding: 10,
        alignItems: 'flex-start'
    },
    Info: {
        marginBottom: 2,
        flexDirection: 'row',
        alignItems: 'center',
        alignSelf: 'stretch',
    },
    lastFourReviewAvatar:
    {
        flexDirection: 'row',
        marginLeft: 15,
        width: 60,
        height: 25,
        alignItems: 'flex-end'
    },
    avatrReview: {
        width: 25,
        height: 25,
        borderRadius: 30,
        borderWidth: 1,
        borderColor: THEME.WHITE_COLOR,
        position: 'absolute',
    },
    contentInfoRounded: {
        marginLeft: 5,
        borderRadius: 20,
    },
    btnPlus: {
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 20,
        height: 30,
        width: 30,
        position: 'absolute',
        bottom: '50%',
        right: 10,
        elevation: 3
    },

})