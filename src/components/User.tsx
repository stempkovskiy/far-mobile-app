import React, { useContext, useState } from 'react'
import { View, StyleSheet, Image } from 'react-native'
import { followAsync, getUserAsync, GetUserEnum, unFollowAsync } from '../api/user'
import { Button } from './UI/Button'
import { AuthContext } from '../context/auth/authContext'
import { THEME } from '../theme'
import { TextRegular } from './UI/TextRegular'
import { AppContext } from '../context/app/appContenxt'

export type UsersListType = {
    subscribe: boolean,
    idFollow: string,
    avatar: string,
    login: string,
    countReview: number
}
export const User: React.FC<UsersListType> = ({ avatar, login, countReview, idFollow, subscribe }) => {
    const [isFollow, setIsFollow] = useState(subscribe)
    const { userInfo, updateUserInfo } = useContext(AppContext)

    const onPress = () => !isFollow ? follow() : unFollow()

    const follow = async () => {
        const response = await followAsync({
            IdFollow: idFollow,
            IdUser: userInfo.id
        })
        if (response.success) {
            updateUser(true)
        }
    }

    const unFollow = async () => {
        const response = await unFollowAsync({
            IdFollow: userInfo.following.find(f => f.idFollowing === idFollow)?.id!,
            IdUser: userInfo.id
        })
        if (response.success) {
            updateUser(false)
        }
    }

    const updateUser = async (isFollow: boolean) => {
        const resposne = await getUserAsync(userInfo.id, GetUserEnum.id)
        if (resposne.success) {
            updateUserInfo(resposne.data?.userInfos[0]!)
            setIsFollow(isFollow)
        }
    }

    return <View style={stylse.container}>
        <Image source={{ uri: avatar }} style={stylse.avatar} />
        <View style={{ justifyContent: 'space-between', alignSelf: 'stretch' }}>
            <TextRegular text={login} fontSize={18} />
            <TextRegular text={`${countReview} Rated Restaurant`} color={THEME.TEXT_COLOR} />
        </View>
        <Button
            btnStyle={{
                backgroundColor: isFollow
                    ? THEME.WHITE_COLOR
                    : THEME.BTN_BACKGROUND_COLOR,
                borderColor: isFollow ? THEME.TEXT_COLOR : 'none',
                borderWidth: isFollow ? 1 : 0
            }}
            colorText={isFollow ? THEME.TEXT_COLOR : THEME.WHITE_COLOR}
            containerStyle={stylse.btn}
            onPress={onPress}
            text={!isFollow ? 'Follow' : 'Unfollow'} />
    </View>
}

const stylse = StyleSheet.create({
    container: {
        width: '100%',
        justifyContent: 'space-around',
        alignItems: 'center',
        backgroundColor: THEME.WHITE_COLOR,
        flexDirection: 'row',
        marginBottom: 10,
    },
    avatar: {
        height: 50,
        width: 50,
        borderRadius: 30
    },
    btn: {
        borderWidth: 1,
        height: 55,
        width: '25%',
        paddingVertical: 10,
        borderRadius: 10,
    }

})