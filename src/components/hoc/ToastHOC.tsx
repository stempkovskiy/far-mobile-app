import React from 'react'
import { View, StyleSheet } from 'react-native'
import { THEME } from '../../theme'
import LottieView from 'lottie-react-native'

export const ToastHOC: React.FC = ({ children }) => {

    return <View style={styles.container}>
        <View style={styles.toast} >
            <LottieView
                autoPlay={true}
                resizeMode='contain'
                source={require('../../../assets/img/check.json')}
            />
        </View>
        {
            children
        }
    </View>
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: THEME.WHITE_COLOR
    },
    toast: {
        zIndex: 10,
        backgroundColor: 'rgba(0, 0, 0, .8)',
        width: '100%',
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center'
    }
})