import React from 'react'
import { ImageBackground, StyleSheet, View } from 'react-native'

interface IImageHOC {
    path: any,
    rgba?: string
}
export const ImageHOC: React.FC<IImageHOC> = ({ path, rgba = 'rgba(0, 0, 0, .5)', children }) => {
    return <ImageBackground
        style={styles.container}
        blurRadius={2}
        source={path}>
        <View style={{ ...styles.content, backgroundColor: rgba }}>
            {
                children
            }
        </View>
    </ImageBackground>
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    content: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    }
})