import React from 'react'
import { View, TouchableOpacity, StyleSheet, ViewStyle } from 'react-native'
import { TextRegular } from './UI/TextRegular'
import { TextBold } from './UI/TextBold'

export type InfoLineType = {
    title: string,
    info: string,
    onPress?: () => void,
    style?: ViewStyle
}
export const InfoLine: React.FC<InfoLineType> = ({ title, info, onPress, style}) => {
    return (
        <View style={{ ...styles.wrap, ...style }}>
            <TextBold text={title} color='black' fontSize={20} />
            <TouchableOpacity onPress={onPress}>
                <TextRegular text={info} color='#B6BED4' fontSize={15} />
            </TouchableOpacity>
        </View>
    )
}


const styles = StyleSheet.create({
    wrap: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        alignSelf: 'stretch'
    }
})