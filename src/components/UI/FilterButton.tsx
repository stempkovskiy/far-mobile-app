import { useNavigation } from '@react-navigation/native'
import React, { useContext } from 'react'
import { Dimensions, View, StyleSheet } from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler'
import { AppContext } from '../../context/app/appContenxt'
import { FilterContext } from '../../context/filter/filterContext'
import { ITrendingRestauransRouteData } from '../../screens/home/TrendingRestaurant'
import { THEME } from '../../theme'
import { filter } from '../../utils/restauransWork'
import { TextRegular } from './TextRegular'
import { ToastTypeEnum } from './Toast'


export interface IFilterButton {
    distance: number
}


export const FilterButton: React.FC<IFilterButton> = ({ distance }) => {
    const { navigate } = useNavigation()
    const { resetFilter, categories, rating } = useContext(FilterContext)
    const { restaurants, initToast } = useContext(AppContext)
    return (
        <View style={styles.container} >
            <TouchableOpacity
                onPress={resetFilter}
                style={styles.btn}
                containerStyle={{ flex: 1 }}>
                <TextRegular text='Reset' color={THEME.WHITE_COLOR} />
            </TouchableOpacity>
            <View style={{ height: '100%', width: .5, backgroundColor: THEME.TEXT_COLOR }} />
            <TouchableOpacity
                onPress={() => {
                    console.log(distance)
                    const array = filter(restaurants, categories, rating, distance)
                    if (!array.length) {
                        initToast({ hide: true, text: 'not founded', type: ToastTypeEnum.error })
                    } else {
                        navigate('Trending', {
                            restaurants: array
                        } as ITrendingRestauransRouteData)
                    }
                }}
                style={styles.btn}
                containerStyle={{ flex: 1 }}>
                <TextRegular text='Apply' color={THEME.WHITE_COLOR} />
            </TouchableOpacity>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        borderTopLeftRadius: 30,
        borderTopRightRadius: 30,
        backgroundColor: THEME.BTN_BACKGROUND_COLOR,
        width: '100%',
        flex: 0.2,
        marginTop: 30,
        flexDirection: 'row',
        paddingHorizontal: 20,
    },
    btn: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    }
})