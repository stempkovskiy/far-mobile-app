import React, { useContext, useState } from 'react'
import { View, StyleSheet } from 'react-native'
import { Feather, Entypo, AntDesign } from '@expo/vector-icons'
import { THEME } from '../../theme'
import { TextInput, TouchableOpacity } from 'react-native-gesture-handler'
import { useNavigation } from '@react-navigation/native'
import { FilterContext } from '../../context/filter/filterContext'

interface ISearch {
    placeholder: string
    onChange: (text: string) => void
    icon: boolean
}
export const Search: React.FC<ISearch> = ({ placeholder, icon, onChange }) => {
    const { navigate } = useNavigation()
    const { isActiveFilter } = useContext(FilterContext)
    const iconRight = icon
        ? <TouchableOpacity activeOpacity={0.8} onPress={() => navigate('Filter')}>
            <Entypo name='sound-mix' size={25}
                color={!isActiveFilter ? THEME.TEXT_COLOR : THEME.BTN_BACKGROUND_COLOR} />
        </TouchableOpacity>
        : <></>
    return (
        <View style={styles.container}>
            <Feather name='search' size={25} color='#B6BED4' />
            <TextInput
                onChangeText={onChange}
                style={styles.textInput}
                placeholder={placeholder} />
            {iconRight}
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        width: '100%',
        padding: 15,
        backgroundColor: THEME.WHITE_COLOR,
        borderRadius: 10,
        flexDirection: 'row',
        borderWidth: 1,
        alignItems: 'flex-start',
        borderColor: '#E8E8E8',
    },
    textInput: {
        fontFamily: THEME.JOSEFINSANS_REGULAR,
        color: '#B6BED4',
        fontSize: 20,
        flex: 1,
        padding: 0,
        marginLeft: 5,
    }
})