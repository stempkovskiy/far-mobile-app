import React, { useState, useEffect, useRef } from 'react'
import {
    View,
    TextInput,
    StyleSheet,
    Dimensions,
    Animated,
    ViewStyle,
    Vibration,
    TouchableOpacity
} from 'react-native'
import {
    SimpleLineIcons,
    MaterialIcons,
} from '@expo/vector-icons'
import { THEME } from '../../theme'
import { TextBold } from './TextBold'
import { ValidationResponse } from '../../validation/IValidatorResponse'

export enum InputIcon {
    email = 'envelope',
    lockOpen = 'lock-open',
    lockClose = 'lock',
    user = 'user'
}


interface IInputProperty {
    password?: string,
    secureTextEntry: boolean,
    icon: InputIcon,
    placeholder: string,
    colorIcon?: string,
    placeholderTextColor?: string,
    validatorsFunc: ((value: string, ...args: any) => ValidationResponse | boolean)[],
    onChange: (value: string) => void,
}


export const Input: React.FC<IInputProperty> = (props) => {

    const initInputStyle: ViewStyle = {
        borderStyle: undefined,
        borderWidth: 0,
        borderColor: 'transparent',
        padding: 10,
        transform: [
            { translateX: 0 }
        ]
    }
    const inputDynamicProp = {
        icon: props.icon,
        secureTextEntry: props.secureTextEntry
    }

    const [animation] = useState<Animated.Value>(new Animated.Value(0))
    const [shakeAnimation] = useState<Animated.Value>(new Animated.Value(0))
    const [inputStyle, setInputStyle] = useState<ViewStyle>(initInputStyle)
    const [repeatAnim, setRepeatAnim] = useState(0)
    const [showPass, setShowPass] = useState(false)
    const [icon, setIcon] = useState(inputDynamicProp)
    const errors = useRef<string[]>([]).current

    const [valid, setValid] = useState<boolean>(false)

    //interpolate properties
    const transformXInterpalation = animation.interpolate({
        inputRange: [0, 1],
        outputRange: [-50, 0]
    })
    const opacityInterpalation = animation.interpolate({
        inputRange: [0, 1],
        outputRange: [0, 1]
    })

    const shakeInterpalation = shakeAnimation.interpolate({
        inputRange: [0, 1, 2, 3],
        outputRange: [0, -5, 5, 0]
    })


    // animation styles
    const animatedStyle = {
        opacity: opacityInterpalation,
        transform: [
            {
                translateX: transformXInterpalation
            }
        ]
    }

    const shakeStyle = {
        transform: [
            {
                translateX: shakeInterpalation
            }
        ]
    }

    // Animations
    const shakeAnim = () => {
        Vibration.vibrate(100)
        Animated.timing(shakeAnimation, {
            toValue: 1,
            duration: 50,
            useNativeDriver: true
        }).start(() => {
            Animated.timing(shakeAnimation, {
                toValue: 2,
                duration: 50,
                useNativeDriver: true
            }).start(() => {
                Animated.timing(shakeAnimation, {
                    toValue: 3,
                    duration: 50,
                    useNativeDriver: true
                }).start()
            })
        })
    }

    const showWrongText = () => {
        Animated.timing(animation, {
            toValue: 1,
            duration: 300,
            useNativeDriver: true,
        }).start()
    }

    const hideWrongText = () => {
        return Animated.timing(animation, {
            toValue: 0,
            duration: 300,
            useNativeDriver: true,
        }).start()
    }

    const validationCheck = (text: string): boolean => {
        if (text.trim()) {
            if (props.validatorsFunc) {
                props.validatorsFunc.forEach(fun => {
                    const response = props.password ? fun(text, props.password) : fun(text)
                    if (response instanceof ValidationResponse) {
                        errors.length = 0
                        errors.push(...response.errorMessage)
                    } else {
                        errors.length = 0
                    }
                })
                if (errors.length) {
                    if (repeatAnim === 0) {
                        showWrongText()
                        shakeAnim()
                        setRepeatAnim(1)
                        return false
                    }
                    setValid(true)
                    setInputStyle(THEME.BORDER_INPUT_ERROR as ViewStyle)
                    return false
                } else {
                    setRepeatAnim(0)
                    setValid(false)
                    hideWrongText()
                    setInputStyle(THEME.BORDER_INPUT_SUCCSES as ViewStyle)
                    return true
                }
            }
            return false
        } else {
            setInputStyle(initInputStyle)
            hideWrongText()
            setValid(false)
            setRepeatAnim(0)
            return false
        }
    }

    const changePassIcon = () => {
        showPass ? setIcon({
            icon: InputIcon.lockClose,
            secureTextEntry: true
        }) : setIcon({
            icon: InputIcon.lockOpen,
            secureTextEntry: false
        })
        setShowPass(!showPass)
    }

    const leftIcon = props.secureTextEntry
        ? <TouchableOpacity activeOpacity={0.5} onPress={changePassIcon}>
            <SimpleLineIcons name={icon.icon} size={20} color={props.colorIcon} />
        </TouchableOpacity>
        : <SimpleLineIcons name={icon.icon} size={20} color={props.colorIcon} />

    const [focus, setFocus] = useState(false)
    return (
        <View style={{ position: 'relative', width: '100%'  }}>
            <Animated.View style={{ ...styles.container, ...inputStyle, ...shakeStyle}}>
                {leftIcon}
                <TextInput
                    secureTextEntry={icon.secureTextEntry && focus}
                    onFocus={() => setFocus(true)}
                    onChange={event => {
                        const text = event.nativeEvent.text
                        const isValid = validationCheck(text)
                        props.onChange(isValid ? text : '')
                    }}
                    placeholder={props.placeholder}
                    placeholderTextColor={props.placeholderTextColor}
                    style={styles.textInput} />
                {
                    valid ?
                        <TouchableOpacity activeOpacity={0.5}>
                            <MaterialIcons name="error" size={20} color="red" />
                        </TouchableOpacity>
                        : null
                }
            </Animated.View>

            <Animated.View
                style={[styles.wrapFroWrongText, animatedStyle]}>
                {
                    errors.map((error, index) =>
                        <TextBold key={index} text={error} fontSize={12} color='red' />)
                }
            </Animated.View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'rgba(255,255,255, 0.22)',
        borderRadius: THEME.BTN_BORDER_RADIUS,
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: 15,
        width: '100%'
    },
    textInput: {
        marginLeft: 6,
        padding: 0,
        flex: 1,
        color: THEME.WHITE_COLOR,
        fontFamily: THEME.JOSEFINSANS_REGULAR
    },
    wrapFroWrongText: {
        position: 'absolute',
        bottom: 0,
    },
})