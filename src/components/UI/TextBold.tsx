import React from 'react'
import { Text, StyleSheet } from 'react-native'
import { THEME } from '../../theme'
import { ITextLabel } from '../../interface/ITextLabel'

export const TextBold: React.FC<ITextLabel> = ({ text, style, color = 'black', fontSize = 14 }) =>
    <Text style={{ ...styless.text, ...style, color, fontSize }}>{text}</Text>


const styless = StyleSheet.create({
    text: {
        fontFamily: THEME.JOSEFINSANS_BOILD,
    }
}
)