import React from 'react'
import { View, StyleSheet, ViewStyle } from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler'
import { TextBold } from '../UI/TextBold'
import { THEME } from '../../theme'
import LottieView from 'lottie-react-native'


interface IButton {
    text: string,
    colorText?: string,
    backgroundColor?: string,
    onPress: Function,
    loading?: boolean,
    containerStyle?: ViewStyle,
    btnStyle?: ViewStyle,
}

export const Button: React.FC<IButton> =
    ({ backgroundColor = THEME.BTN_BACKGROUND_COLOR, text, onPress, loading, containerStyle, btnStyle, colorText }) => {
        return (
            <TouchableOpacity
                containerStyle={{ width: '100%', height: 50, ...containerStyle }}
                disabled={loading}
                activeOpacity={0.8}
                onPress={() => onPress()}>
                <View style={{
                        ...styles.container,
                        backgroundColor: !loading ? backgroundColor : '#696D9F',
                        ...btnStyle
                    }}>
                    {
                        !loading
                            ? <TextBold text={text} color={colorText ? colorText : THEME.WHITE_COLOR} />
                            : <LottieView
                                autoPlay={true}
                                style={{ height: 80 }}
                                source={require('../../../assets/img/loading5.json')} />
                    }
                </View>
            </TouchableOpacity>
        )
    }

const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%',
        height: '100%',
        borderRadius: THEME.BTN_BORDER_RADIUS,
    }
})