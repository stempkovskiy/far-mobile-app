import React, { useContext, useEffect, useRef } from 'react'
import Animated, { useAnimatedStyle, withTiming, useSharedValue, Easing } from 'react-native-reanimated'
import { Dimensions, LayoutChangeEvent, StyleSheet } from 'react-native'
import { TextRegular } from './TextRegular'
import { THEME } from '../../theme'
import { AppContext } from '../../context/app/appContenxt'

export type ToastResponse = {
    text: string,
    type: ToastTypeEnum,
    hide: boolean
}

export enum ToastTypeEnum {
    success,
    error
}

type ToastProperty = {
    top?: number
    onClick: () => void
}
export const Toast: React.FC<ToastProperty> = ({ onClick, top = 0 }) => {
    const { toast } = useContext(AppContext)
    const translateX = useSharedValue(0)
    const opacity = useSharedValue(0)
    const prevX = useRef(false)
    const width = useRef(0)
    const style = useAnimatedStyle(() => {
        return {
            transform: [{
                translateX: withTiming(translateX.value, {
                    duration: 200,
                    easing: Easing.ease
                })
            }],
            zIndex: withTiming(opacity.value),
            opacity: withTiming(opacity.value)
        }
    })

    useEffect(() => hide(), [toast.hide])

    const hide = () => {
        if (toast.hide) {
            prevX.current = true
            const window = Dimensions.get('window').width
            translateX.value = (window - width.current) / 2
            opacity.value = 1
        } else {
            if (prevX.current) {
                translateX.value = 0
                opacity.value = 0
                prevX.current = false
            }
        }
    }

    return (
        <Animated.View
            style={[
                styles.container,
                style,
                {
                    top,
                    backgroundColor: toast.type === ToastTypeEnum.success
                        ? THEME.COLOR_SUCCESS
                        : THEME.COLOR_ERROR
                },
            ]}
            onStartShouldSetResponder={() => true}
            onResponderStart={onClick}
            onLayout={(event: LayoutChangeEvent) =>
                width.current = event.nativeEvent.layout.width}>
            <TextRegular
                text={toast.text}
                color={THEME.WHITE_COLOR}
                fontSize={15} />
        </Animated.View>
    )
}

const styles = StyleSheet.create({
    container: {
        padding: 10,
        position: 'absolute',
        borderRadius: 10,
        width: '90%',
        left: 0,
        justifyContent: 'space-between',
        alignItems: 'center',
        flexDirection: 'row',
        flexWrap: 'wrap',
        zIndex: 1
    }
})