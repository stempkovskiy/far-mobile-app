import React from 'react'
import { Text, StyleSheet } from 'react-native'
import { THEME } from '../../theme'
import { ITextLabel } from '../../interface/ITextLabel'

export const TextRegular: React.FC<ITextLabel> = ({ text, style, color, fontSize = 14 }) =>
    <Text style={{ ...styles.text, ...style, color, fontSize }}>{text}</Text>


const styles = StyleSheet.create({
    text: {
        fontFamily: THEME.JOSEFINSANS_REGULAR
    }
})