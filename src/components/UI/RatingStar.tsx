import { FontAwesome } from '@expo/vector-icons'
import React, { useContext, useEffect, useState } from 'react'
import { Animated, GestureResponderEvent, StyleSheet, View } from 'react-native'
import { FilterContext, initStarsFilter, Rating, RatingEnum, Star } from '../../context/filter/filterContext'


export interface IRatingStarType {
    getRating: (rating: Rating) => void,
    isFilterStars: boolean,
}
export const RatingStar: React.FC<IRatingStarType> = ({ getRating, isFilterStars }) => {
    const { stars } = useContext(FilterContext)
    const initValue: Star[] = [
        { id: 0, full: RatingEnum.empty },
        { id: 1, full: RatingEnum.empty },
        { id: 2, full: RatingEnum.empty },
        { id: 3, full: RatingEnum.empty },
        { id: 4, full: RatingEnum.empty }
    ]
    const [localStars, setLocalStars] = useState<Star[]>(initValue)
    const setStarRating = (x: number, id: number) => {
        let updateStars = isFilterStars ? stars : initValue
        updateStars = updateStars.map((star, _) => {
            if (id > star.id)
                star.full = RatingEnum.full
            if (id < star.id)
                star.full = RatingEnum.empty
            if (star.id === id) {
                if (x >= 10 && x <= 25) {
                    star.full = RatingEnum.half
                } else if (x >= 0 && x < 10) {
                    star.full = RatingEnum.empty
                } else {
                    star.full = RatingEnum.full
                }
            }
            return star
        })
        setLocalStars(updateStars)
        calculateRating(updateStars)
    }

    const calculateRating = (array: Star[]) => {
        const init: Rating = {
            stars: initValue,
            value: 0
        }
        const newRating = array.reduce<Rating>((prev, current, index) => {
            switch (current.full) {
                case 0:
                    prev.value += 0
                    prev.stars[index].full = RatingEnum.empty
                    return prev
                case 1:
                    prev.value += .5
                    prev.stars[index].full = RatingEnum.half
                    return prev
                case 2:
                    prev.value += 1
                    prev.stars[index].full = RatingEnum.full
                    return prev
            }
            return prev
        }, init)
        getRating(newRating)
    }

    const drawStar = (state: RatingEnum) => {
        switch (state) {
            case RatingEnum.empty:
                return <FontAwesome name='star' size={40} color='#e1e6d5' />
            case RatingEnum.full:
                return <FontAwesome name='star' size={40} color='#fca903' />
            case RatingEnum.half:
                return <FontAwesome name='star-half-empty' size={40} color='#fca903' />
        }
    }

    const drawContent = (stars: Star[]) =>
        stars
            .map((star, index) =>
                <Animated.View style={styles.star}
                    key={index}
                    onStartShouldSetResponder={() => true}
                    onResponderGrant={(event: GestureResponderEvent) =>
                        setStarRating(event.nativeEvent.locationX, stars[index].id)}>
                    {drawStar(star.full)}
                </Animated.View>)

    return <View style={styles.contentRating}>
        {
            isFilterStars
                ? drawContent(stars)
                : drawContent(localStars)
        }
    </View>
}

const styles = StyleSheet.create({
    contentRating: {
        flexDirection: 'row',
        marginTop: 20,
        borderRadius: 10,
        backgroundColor: '#EEF7FF',
        justifyContent: 'space-evenly',
        padding: 10
    },
    star: {
        width: 50,
        height: 50,
        justifyContent: 'center',
        alignItems: 'center',
    },

})
