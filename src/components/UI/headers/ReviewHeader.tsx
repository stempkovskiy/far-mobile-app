import React from 'react'
import { StyleSheet, View, TouchableOpacity, Dimensions } from 'react-native'
import { TextBold } from '../../../components/UI/TextBold'
import { THEME } from '../../../theme'
import { TextRegular } from '../TextRegular'

export interface IReviewHrader {
    headerRight: () => void,
    headerLeft: () => void
}

export const ReviewHeader: React.FC<IReviewHrader> = ({headerLeft, headerRight}) => {
    return (
        <View style={styles.container} >
            <TouchableOpacity
                style={styles.btn}
                activeOpacity={.8}
                onPress={headerLeft}
            >
                <TextRegular text='Cancel' fontSize={18} color={THEME.TEXT_COLOR} />
            </TouchableOpacity>
            <View style={styles.title}>
                <TextBold text='New Review' fontSize={18} />
            </View>
            <TouchableOpacity
                style={styles.btn}
                activeOpacity={.8}
                onPress={headerRight}
            >
                <TextRegular text='Post' fontSize={18} color={THEME.TEXT_COLOR} />
            </TouchableOpacity>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        justifyContent: 'space-around',
        alignItems: 'center',
        flexDirection: 'row',
        width: '100%',
        height: Dimensions.get('window').height / 7,
    },
    title: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    btn: {
        flex: .5,
        alignSelf: 'stretch',
        justifyContent: 'center',
        alignItems: 'center'
    },
})