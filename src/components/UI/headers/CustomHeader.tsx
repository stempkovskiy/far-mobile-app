import { StackHeaderProps } from '@react-navigation/stack'
import React, { useContext, useState } from 'react'
import { StyleSheet, View } from 'react-native'
import { AnimatedSearchHeader } from './AnimatedSearchHeader'
import { Search } from '../Search'
import { THEME } from '../../../theme'
import { ReviewHeader } from './ReviewHeader'
import { AppContext } from '../../../context/app/appContenxt'
import { TextRegular } from '../TextRegular'
type HeaderType = {
    props: StackHeaderProps
}
export const CustomHeader: React.FC<HeaderType> = ({ props }) => {
    const index = props.scene.route.state?.index || 0
    const { searchRestourantsAtHomeScreen, restaurants } = useContext(AppContext)
    const onChange = (text: string) => {
        if (text.trim()) {
            const filtredRestairants = restaurants
                .filter(r => r.name.includes(text))
            searchRestourantsAtHomeScreen(filtredRestairants)
        } else {
            searchRestourantsAtHomeScreen([])
        }
    }
    switch (index) {
        case 0:
            return <View style={styles.container}>
                <Search
                    onChange={onChange}
                    placeholder='Find Restaurant'
                    icon={true} />
            </View>
        case 1:
            return <View style={styles.container}>
                <AnimatedSearchHeader />
            </View>
        case 4:
            return <View style={styles.profile}>
                <TextRegular text='My Profile' fontSize={20} />
            </View>
        default:
            return <></>
    }
}

const styles = StyleSheet.create({
    container: {
        paddingHorizontal: 10,
        backgroundColor: THEME.WHITE_COLOR,
        alignItems: 'center',
        justifyContent: 'flex-end',
        height: 100
    },
    profile: {
        backgroundColor: THEME.WHITE_COLOR,
        justifyContent: 'center',
        alignItems: 'center',
        paddingVertical: 30
    }
})
