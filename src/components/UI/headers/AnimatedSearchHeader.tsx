import { Feather } from '@expo/vector-icons'
import React, { useState } from 'react'
import { View, StyleSheet, TouchableOpacity, TextInput, Animated, Dimensions } from 'react-native'
import { THEME } from '../../../theme'
import { TextRegular } from '../TextRegular'

export const AnimatedSearchHeader: React.FC = () => {
    const width = useState(new Animated.Value(0))[0]
    const [title, setTitle] = useState('My Favourite')
    const [isChange, setChange] = useState(false)
    const animatedWidth = () => {
        setTitle(!isChange ? '' : 'My Favourite')
        Animated.spring(width, {
            toValue: !isChange ? 90 : 0,
            useNativeDriver: false,
        }).start(() => setChange(!isChange))
    }
    return (
        <View style={styles.header}>
            <View style={styles.title}>
                <TextRegular text={title} fontSize={20} />
            </View>
            <Animated.View style={
                {
                    ...styles.inputWrap,
                    width: width.interpolate({
                        inputRange: [0, 90],
                        outputRange: ['15%', '100%']
                    }),
                    borderWidth: width.interpolate({
                        inputRange: [0, 90],
                        outputRange: [0, 1]
                    }),
                    borderRadius: width.interpolate({
                        inputRange: [0, 90],
                        outputRange: [0, 15]
                    }),
                    borderColor: width.interpolate({
                        inputRange: [0, 90],
                        outputRange: ['transparent', THEME.TEXT_COLOR]
                    }),
                }} >
                <TouchableOpacity
                    activeOpacity={.5}
                    onPress={() => animatedWidth()}
                    style={styles.btnSearch}>
                    <Feather name="search" size={24} color="black" />
                </TouchableOpacity>
                <TextInput style={styles.input} />
            </Animated.View>
        </View>
    )
}

const styles = StyleSheet.create({
    header: {
        top: 0,
        width: '100%',
        height: Dimensions.get('window').height / 6,
        justifyContent: 'flex-end',
        alignItems: 'center',
        paddingHorizontal: 20,
        flexDirection: 'row',
        backgroundColor: THEME.WHITE_COLOR
    },
    title: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        justifyContent: 'center',
        alignItems: 'center'
    },
    btnSearch: {
        marginLeft: 10,
        flex: 1,
        justifyContent: 'center',
        width: 50,
    },
    inputWrap: {
        height: '50%',
        width: '15%',
        position: 'relative',
    },
    input: {
        position: 'absolute',
        right: 0,
        bottom: 0,
        top: 0,
        left: 40,
    }
})