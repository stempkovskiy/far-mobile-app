import { AntDesign, Ionicons } from '@expo/vector-icons'
import { useNavigation } from '@react-navigation/native'
import React, { useContext, useState } from 'react'
import { StyleSheet, TouchableOpacity, View } from 'react-native'
import { TextRegular } from '../TextRegular'
import { THEME } from '../../../theme'
import { StackHeaderProps } from '@react-navigation/stack'
import { AppContext } from '../../../context/app/appContenxt'
import { ToastTypeEnum } from '../Toast'
type MyHeaderType = {
    props?: StackHeaderProps
    title: string
    showBtnClose?: boolean
}
export const MyHeader: React.FC<MyHeaderType> = ({ title, showBtnClose = true, props }) => {
    const navigation = useNavigation()
    const { initToast, toast } = useContext(AppContext)
    return <View style={styles.container}>
        <TouchableOpacity style={styles.back} onPress={() => {
            initToast({...toast, hide: false})
            navigation.goBack()
        }}>
            <Ionicons name='ios-arrow-back' size={30} />
        </TouchableOpacity>
        <View style={styles.title}>
            <TextRegular text={props ? props.scene.descriptor.options.title! : title} fontSize={20} />
        </View>
        <View style={{width: '30%'}}/>
    </View>
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: THEME.WHITE_COLOR,
        height: 80,
        justifyContent: 'space-between',
        alignItems: 'center',
        flexDirection: 'row',
    },
    back: {
        width: '30%',
        alignSelf: 'stretch',
        justifyContent: 'center',
        alignItems: 'center',
    },
    title: {
        flexDirection: 'row',
        alignItems: 'center',
    },
})