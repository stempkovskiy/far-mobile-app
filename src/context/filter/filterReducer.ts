import { SET_CATEGORIES, SET_PATH, CHANGE_RATING_STARS, RESET_FILTER } from '../../types'
import {  RatingEnum, Star } from './filterContext'

export type InitStateFilter = {
    categories: string[]
    rating: number
    isActiveFilter: boolean
    stars: Star[]
    distance: number
}

export type ActionPropertyFilter = {
    isActiveFilter?: boolean,
    categories?: string[]
    stars?: Star[]
    rating?: number
    type: string
    distance?: number
}

type HandlerTypeFilter = {
    [key: string]: (...args: any) => InitStateFilter
}

const handlers: HandlerTypeFilter = {
    [SET_CATEGORIES]: (state: InitStateFilter, action: ActionPropertyFilter): InitStateFilter =>
        ({ ...state, categories: action.categories!, isActiveFilter: true }),
    [SET_PATH]: (state: InitStateFilter, { distance }: ActionPropertyFilter): InitStateFilter =>
        ({ ...state, distance: distance!, isActiveFilter: true }),
    [CHANGE_RATING_STARS]: (state: InitStateFilter, action: ActionPropertyFilter): InitStateFilter =>
        ({ ...state, stars: action.stars!, rating: action.rating!, isActiveFilter: true }),
    [RESET_FILTER]: (state: InitStateFilter): InitStateFilter =>
        ({
            ...state,
            categories: [],
            isActiveFilter: false,
            distance: 0,
            stars: [
                { id: 0, full: RatingEnum.empty },
                { id: 1, full: RatingEnum.empty },
                { id: 2, full: RatingEnum.empty },
                { id: 3, full: RatingEnum.empty },
                { id: 4, full: RatingEnum.empty }
            ]
        }),
    DEFAULT: (state: InitStateFilter) => state
}

export const appReducer = (state: InitStateFilter, action: ActionPropertyFilter) => {
    const handler = handlers[action.type] || handlers.DEFAULT
    return handler(state, action)
}