import { createContext } from 'react'

export enum RatingEnum {
    empty,
    half,
    full
}

export type Star = {
    id: number,
    full: RatingEnum
}

export type Rating = {
    stars: Star[]
    value: number
}


type FilterContextType = {
    setCategories: (categories: string[]) => void
    setDistance: (distance: number) => void
    setRating: (start: Star[], rating: number) => void
    resetFilter: () => void
    isActiveFilter: boolean
    distance: number
    categories: string[]
    rating: number,
    stars: Star[]
}


export const initStarsFilter: Star[] = [
    { id: 0, full: RatingEnum.empty },
    { id: 1, full: RatingEnum.empty },
    { id: 2, full: RatingEnum.empty },
    { id: 3, full: RatingEnum.empty },
    { id: 4, full: RatingEnum.empty }
]

export const FilterContext = createContext({} as FilterContextType)