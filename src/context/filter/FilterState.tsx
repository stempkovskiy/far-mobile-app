import React, { useReducer } from 'react'
import { FilterContext, initStarsFilter, Star } from './filterContext'
import { appReducer, InitStateFilter } from './filterReducer'
import { CHANGE_FILTER_STATUS, CHANGE_RATING_STARS, RESET_FILTER, SET_CATEGORIES, SET_PATH } from '../../types'
export const FilterState: React.FC = ({ children }) => {
    const initValue: InitStateFilter = {
        distance: 0,
        stars: initStarsFilter,
        isActiveFilter: false,
        rating: 0,
        categories: [],
    }
    const [state, dispatch] = useReducer(appReducer, initValue)
    const setCategories = (categories: string[]) => dispatch({ type: SET_CATEGORIES, categories })
    const setRating = (stars: Star[], rating: number) =>
        dispatch({ type: CHANGE_RATING_STARS, stars, rating })
    const setDistance = (distance: number) => dispatch({ type: SET_PATH, distance })
    const resetFilter = () => dispatch({ type: RESET_FILTER })


    return (
        <FilterContext.Provider
            value={{
                isActiveFilter: state.isActiveFilter,
                categories: state.categories,
                distance: state.distance,
                stars: state.stars,
                rating: state.rating,
                setRating,
                setCategories,
                resetFilter,
                setDistance
            }}>
            {children}
        </FilterContext.Provider>
    )
} 