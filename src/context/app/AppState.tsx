import React, { ReactNode, useReducer } from 'react'
import { appReducer, InitStateApp } from './appReducer'
import { AppContext, ToastType, UserInfo, Location } from './appContenxt'
import {
    ADD_FAVOURITE,
    REMOVE_FAVOURITE,
    SET_RESTAURANTS,
    SET_LOADING, SEARCH_AT_HOME_RESTAURANTS, SHOW_TOAST, UPDATE_USER_INFO, SET_LOCATION
} from '../../types'
import { Restauran } from '../../api/restauran'
import { ToastTypeEnum } from '../../components/UI/Toast'


export const AppState: React.FC<ReactNode> = ({ children }) => {
    const initApp: InitStateApp = {
        userInfo: {
            avatar: { name: '', path: '', id: '' },
            email: '',
            followers: [],
            following: [],
            id: '',
            login: '',
            reviews: [],
            userId: ''
        },
        restaurants: [],
        searchAtHomeRestaursnts: [],
        loading: false,
        toast: {
            hide: false,
            text: '',
            type: ToastTypeEnum.success
        },
        location: { latitude: 0, longitude: 0 }
    }
    const [state, dispatch] = useReducer(appReducer, initApp)
    const updateUserInfo = (userInfo: UserInfo) => dispatch({ type: UPDATE_USER_INFO, userInfo })
    const setRestourants = (restaurants: Restauran[]) => dispatch({ type: SET_RESTAURANTS, restaurants })
    const setLoading = (loading: boolean) => dispatch({ type: SET_LOADING, loading })
    const setLocation = (location: Location) => dispatch({type: SET_LOCATION, location})
    const addFavourite = (restaurant: Restauran) => dispatch({ type: ADD_FAVOURITE, restaurant })
    const removeFavourite = (restaurant: Restauran) => dispatch({ type: REMOVE_FAVOURITE, restaurant })
    const searchRestourantsAtHomeScreen = (searchAtHomeRestaursnts: Restauran[]) =>
        dispatch({ type: SEARCH_AT_HOME_RESTAURANTS, searchAtHomeRestaursnts })
    const initToast = ({ hide, type, text }: ToastType) =>
        dispatch({ type: SHOW_TOAST, toast: { hide, type, text } })
    return (
        <AppContext.Provider value={{
            initToast,
            setLoading,
            setLocation,
            addFavourite,
            setRestourants,
            removeFavourite,
            updateUserInfo,
            searchRestourantsAtHomeScreen,
            location: state.location,
            userInfo: state.userInfo,
            restaurants: state.restaurants,
            loading: state.loading,
            toast: state.toast,
            searchAtHomeRestaursnts: state.searchAtHomeRestaursnts
        }}>
            {children}
        </AppContext.Provider>
    )
}