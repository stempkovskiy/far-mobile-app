import { Restauran } from '../../api/restauran'
import {
    SET_RESTAURANTS,
    ADD_FAVOURITE,
    REMOVE_FAVOURITE,
    SET_LOADING,
    SEARCH_AT_HOME_RESTAURANTS,
    SHOW_TOAST,
    UPDATE_USER_INFO,
    SET_LOCATION
} from '../../types'
import { Location, ToastType, UserInfo } from './appContenxt'

export type InitStateApp = {
    restaurants: Restauran[]
    searchAtHomeRestaursnts: Restauran[]
    userInfo: UserInfo
    loading: boolean
    location: Location
    toast: ToastType
}

export type ActionPropertyApp = {
    type: string
    userInfo?: UserInfo
    restaurants?: Restauran[]
    searchAtHomeRestaursnts?: Restauran[]
    restaurant?: Restauran
    loading?: boolean
    location?: Location
    toast?: ToastType
}

type HandlerTypeApp = {
    [key: string]: (...args: any) => InitStateApp
}

const changeIsFavorite = (restaurants: Restauran[], value: boolean, id: string) => {
    return restaurants.map(r => {
        if (r.id === id) {
            r.isFavouvarit = value
            return r
        }
        return r
    })
}

const handlers: HandlerTypeApp = {
    [SEARCH_AT_HOME_RESTAURANTS]: (state: InitStateApp, action: ActionPropertyApp): InitStateApp =>
        ({ ...state, searchAtHomeRestaursnts: action.searchAtHomeRestaursnts! }),
    [SET_RESTAURANTS]: (state: InitStateApp, action: ActionPropertyApp): InitStateApp =>
        ({ ...state, restaurants: action.restaurants! }),
    [ADD_FAVOURITE]: (state: InitStateApp, action: ActionPropertyApp): InitStateApp => {
        let restaurants = [...state.restaurants]
        restaurants = changeIsFavorite(restaurants, true, action.restaurant?.id!)
        return ({ ...state, restaurants })
    },
    [UPDATE_USER_INFO]: (state: InitStateApp, action: ActionPropertyApp): InitStateApp =>
        ({ ...state, userInfo: action.userInfo! }),
    [REMOVE_FAVOURITE]: (state: InitStateApp, action: ActionPropertyApp): InitStateApp => {
        let restaurants = [...state.restaurants]
        restaurants = changeIsFavorite(restaurants, false, action.restaurant?.id!)
        return ({ ...state, restaurants })
    },
    [SET_LOADING]: (state: InitStateApp, action: ActionPropertyApp): InitStateApp =>
        ({ ...state, loading: action.loading! }),
    [SHOW_TOAST]: (state: InitStateApp, action: ActionPropertyApp): InitStateApp =>
        ({ ...state, toast: action.toast! }),
    [SET_LOCATION]: (state: InitStateApp, action: ActionPropertyApp): InitStateApp =>
        ({ ...state, location: action.location!}),
    DEFAULT: (state: InitStateApp) => state
}

export const appReducer = (state: InitStateApp, action: ActionPropertyApp) => {
    const handler = handlers[action.type] || handlers.DEFAULT
    return handler(state, action)
}