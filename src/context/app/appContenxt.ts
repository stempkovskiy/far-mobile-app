import { createContext } from 'react'
import { Photo, Restauran, Reviews } from '../../api/restauran'
import { ToastTypeEnum} from '../../components/UI/Toast'

interface AppContextProps {
    loading: boolean
    toast: ToastType
    restaurants: Restauran[]
    userInfo: UserInfo
    location: Location
    searchAtHomeRestaursnts: Restauran[]
    updateUserInfo: (userInfo: UserInfo) => void
    searchRestourantsAtHomeScreen: (restaurants: Restauran[]) => void
    setRestourants: (restaurants: Restauran[]) => void
    setLoading: (loading: boolean) => void
    setLocation: (location: Location) => void
    addFavourite: (restaurant: Restauran) => void
    removeFavourite: (restaurant: Restauran) => void
    initToast: (toast: ToastType) => void
}

export type ToastType = {
    text: string, 
    type: ToastTypeEnum, 
    hide: boolean
}

export type UserInfo = {
    id: string,
    login: string,
    email: string,
    avatar: Photo,
    following: FollowingType[]
    followers: Follower[],
    reviews: Reviews[],
    userId: string
}

export interface Location {
    latitude: number,
    longitude: number
}

export type FollowingType = {
    id: string,
    login:string,
    countReview: string,
    avatar: string,
    idFollowing: string,
    userInfoId: string
}

export type Follower = {
    id: string,
    login: string,
    countReview: string,
    avatar: string,
    idFollow: string
}

export const AppContext = createContext({} as AppContextProps)