import { SIGN_IN, SIGN_OUT, SET_TOKEN } from "../../types"

export type InitStateAuth = {
    token: string,
    refreshToken: string
}

export type ActionProperty = {
    type: string,
    token?: string,
    refreshToken?: string
}

type HandlersType = {
    [key: string]: (...args: any) => InitStateAuth
}

const handlers: HandlersType = {
    [SIGN_IN]: (state: InitStateAuth, action: ActionProperty): InitStateAuth =>
        ({ ...state, ...action }),
    [SIGN_OUT]: (state: InitStateAuth): InitStateAuth =>
        ({ ...state, token: '', refreshToken: '' }),
    [SET_TOKEN]: (state: InitStateAuth, action: ActionProperty): InitStateAuth =>
        ({...state, token: action.token!}),
    DEFAULT: (state: InitStateAuth) => state
}

export const authReducer = (state: InitStateAuth, action: ActionProperty) => {
    const hendler = handlers[action.type] || handlers.DEFAULT
    return hendler(state, action)
}