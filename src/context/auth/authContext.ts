import { createContext } from 'react'
import { IUserRegistrationRequest } from '../../api/authorization'

interface AuthContextProps {
    token: string,
    refreshToken: string,
    signIn: (email: string, password: string) => void,
    signOut: () => void,
    registration: (body: IUserRegistrationRequest) => Promise<string[]> ,
}

export const AuthContext = createContext({} as AuthContextProps)