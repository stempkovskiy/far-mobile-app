import React, { ReactNode, useContext, useEffect, useReducer } from 'react'
import { AuthContext } from './authContext'
import { authReducer, InitStateAuth } from './authReducer'
import AsyncStorage from '@react-native-community/async-storage'
import { SET_TOKEN, SIGN_IN, SIGN_OUT } from '../../types'
import { signInAsync, IUserRegistrationRequest, registrationAsync } from '../../api/authorization'
import { getAllRestauranAsync } from '../../api/restauran'
import { AppContext } from '../app/appContenxt'
import { ToastTypeEnum } from '../../components/UI/Toast'
import { deleteAllAsyncStorageValue, setAsyncStorageTokens } from '../../utils/asyncStorageWorker'
import { getUserAsync, GetUserEnum } from '../../api/user'

export const AuthState: React.FC<ReactNode> = ({ children }) => {
    const { setRestourants, setLoading, initToast, updateUserInfo, setLocation } = useContext(AppContext)
    const [auth, dispatch] = useReducer(authReducer, {
        refreshToken: '',
        token: ''
    } as InitStateAuth)

    useEffect(() => {
        AsyncStorage.getItem('@fp_token')
            .then(async storeToken => {
                if (storeToken) {
                    dispatch({ type: SET_TOKEN, token: storeToken })
                    const response = await getAllRestauranAsync()
                    if (response.success) {
                        setRestourants(response.data?.restaurans!)
                        const email = await AsyncStorage.getItem('@fp_email')
                        const userInfo = await getUserAsync(email!, GetUserEnum.email)
                        if (userInfo.success) {
                            updateUserInfo(userInfo.data?.userInfos[0]!)
                        }
                    }
                }
            })
    }, [])


    const signIn = async (email: string, password: string) => {
        try {
            setLoading(true)
            const response = await signInAsync({ Email: email, Password: password })
            if (!response.errors) {
                await setAsyncStorageTokens(response.token, response.refreshToken)
                await AsyncStorage.setItem('@fp_email', response.userInfo.email)
                const restauran = await getAllRestauranAsync()
                if (restauran.success) {
                    setRestourants(restauran.data?.restaurans!)
                    setLoading(false)
                    updateUserInfo(response.userInfo)
                    dispatch({
                        type: SIGN_IN,
                        refreshToken: response.refreshToken,
                        token: response.token
                    })
                } else {
                    showErrorMessage(restauran.message![0])
                }
            } else {
                showErrorMessage(response.errors[0])
            }
        } catch (error) {
            setLoading(false)
        }
    }


    const showErrorMessage = (text: string) => {
        initToast({ hide: true, text, type: ToastTypeEnum.error })
        setLoading(false)
    }

    const registration = async (body: IUserRegistrationRequest): Promise<string[]> => {
        try {
            setLoading(true)
            const response = await registrationAsync(body)
            if (response.errors) {
                setLoading(false)
                return response.errors
            }
            setLoading(false)
            return []
        } catch (error) {
            setLoading(false)
            return []
        }
    }


    const signOut = async () => {
        await deleteAllAsyncStorageValue()
        dispatch({
            type: SIGN_OUT,
            token: '',
            refreshToken: ''
        })
    }



    return (
        <AuthContext.Provider value={{
            signIn,
            signOut,
            registration,
            token: auth.token,
            refreshToken: auth.refreshToken
        }}>
            {children}
        </AuthContext.Provider>
    )
}